<?php
/**
 * @file
 * Contains \Drupal\biopama_conservation_tracking\Controller\ConservationTrackingController.
 */

namespace Drupal\biopama_conservation_tracking\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the biopama_conservation_tracking module.
 */
class ConservationTrackingController extends ControllerBase {
  public function content() {
    $element = array(
      '#theme' => 'conservation_tracking',
    );
    return $element;
  }
}