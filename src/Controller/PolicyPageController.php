<?php
/**
 * @file
 * Contains \Drupal\biopama_conservation_tracking\Controller\PolicyPageController.
 */

namespace Drupal\biopama_conservation_tracking\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the biopama_conservation_tracking module.
 */
class PolicyPageController extends ControllerBase {
  public function content() {
    $element = array(
      '#theme' => 'policy_page',
    );
    return $element;
  }
}