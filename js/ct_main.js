//the global variables that we use in other scripts
var thisMap;  //the map
var tTipDelay = 100;
//var mapContainer = 'map-container';
var mapboxTour;
var indictorGlobalSettings = {
    
}
//indicatorCharts = {};
// indictorGlobalSettings["nodeID"].customClassMethod = '';
// indictorGlobalSettings["nodeID"].mapColors;
// indictorGlobalSettings["nodeID"].firstChartRun = 1;
// indictorGlobalSettings["nodeID"].chartSeriesGlobal = [];
// indictorGlobalSettings["nodeID"].mapCustomSwatchCat = "diverging";
// indictorGlobalSettings["nodeID"].mapCustomSwatchColors = 7;
// indictorGlobalSettings["nodeID"].selectedStopPoints = [];
// indictorGlobalSettings["nodeID"].mapCustomSwatchNum = 0;
// indictorGlobalSettings["nodeID"].customStopPoints = [];
// indictorGlobalSettings["nodeID"].customColorInvert = null;
// indictorGlobalSettings["nodeID"].mapMarkers = [];
// indictorGlobalSettings["nodeID"].userPointToggle = 0;
// indictorGlobalSettings["nodeID"].paintProp;		//this is for the Data layer to have the paint property toggleable
var regionCurrentlyHovered = '';
var countryCurrentlyHoverediso2 = '';
var countryCurrentlyHoverediso3 = '';
var countryCurrentlyHoveredUn = '';
var pasCurrentlyHovered = [];
var currentTab;
var indicatorAjaxRequest = '';

var currentCardScope;
//var currentIndicatorNodeURL;

/* 
Chart 
*/
var indicatorChart;
var chartSettings = {};

var tourSelectCountry, tourSpatialScale, tourSelectPolicy, tourSelectTarget, tourSelectIndicator;


//this is to flag if the user is coming in directly to a country URL to help highlight that country.
var firstVisit = 1;
var regionChanged = 0;
var countryChanged = 0;
var paChanged = 0;
var chart3D = false;
var myCharts = [];
var parts = [];

var numberOfIndicatorsToPrint = 0;

jQuery(document).ready(function($) {
    Drupal.behaviors.biopama_ct_progressbar = {
        attach: function(context, settings) {
       
            $(document).ajaxComplete(function (event, xhr, settings) {
                $('.ct-progress-bar').hide();
            });   

            // $('#country-selector').on('change', function() {
            //     selSettings.ISO2 = $(this).val();
            //     countryChanged = 1;             
            //     $().mapZoomToCountryIso2(thisMap, selSettings.ISO2);
            // });

        }
    };
    Drupal.Ajax.prototype.beforeSubmit = function (form_values, element, options) {
        $('.ct-progress-bar').show();
    };

    Drupal.theme.ajaxProgressThrobber = function () {
        $('.ct-progress-bar').show();
        return;
    };
});