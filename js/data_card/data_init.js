
(function ($, Drupal) {
	Drupal.behaviors.dataInit = {
		attach: function (context, settings) {
			$('div.indicator-content-wrapper', context).once('indicator-loaded').each(function () { 

			if ($('.view-conservation-tracking-menu').length > 1) {
				$().removeBiopamaLoader(".view-conservation-tracking-menu"); 
			}

			var nodeID = "";

			if ($(this).find(".node_id").length){
				nodeID = $(this).find(".node_id").text().trim();
			}

			jQuery().removeBiopamaLoader("#view-wrapper-"+nodeID); 

			var cardWrapper = "#id-" + nodeID;

			// indictorGlobalSettings[nodeID] = {
			// 	info: {
			// 		name: '',
			// 		scope: '',
			// 		policy: '',
			// 		target: '',
			// 		spr: '',
			// 		sprCat: '',
			// 		countries: '',
			// 		targetNum: null,
			// 		qualifier: null,
			// 	},
			// 	data: {
			// 		countries: '',
			// 	},
			// 	map: {
			// 		mapLayerField: '',
			// 		mappedField: '',
			// 		mapPoints: '',
			// 		mapScope: '',
			// 		regionActive: 0,
			// 	},
			// 	chart: {
			// 		pie: {
			// 			type: '',
			// 			limit: 0,
			// 			limitOverLabel: '',
			// 			limitUnderLabel: '',
			// 			unit: ''
			// 		},
			// 		invertColors: '',
			// 		chartType: '',
			// 		dataSource: '',
			// 		RESTurl: '',
			// 		RESTdataContext: '',
			// 		RESTfilter: '',
			// 		RESTFields: '',
			// 		RESTFieldLabels: '',
			// 		RESTResults: '',
			// 		Xaxis: '',
			// 		Yaxis: '',
			// 		chartSeries: '',
			// 		sort: '',
			// 		chartRadarSeries: '',
			// 		chartRadarSettings: '',
			// 		ranking: '',
			// 		areaIn: '',
			// 	},
			// };
			indictorGlobalSettings[nodeID] = {
				chart: {
					pie: {}
				},
				data: {},
				info: {},
				map: {},
			}

			$(cardWrapper + '#indicator-card-tabs-'+nodeID).on('hide.bs.tab', function (e) {
				//e.stopPropagation();
				togglelayergroup('none', nodeID);
			});
			
			$(cardWrapper + ' #indicator-card-tabs-'+nodeID).on('shown.bs.tab', function (e) {
				//e.stopPropagation();
				var s = document.getElementById("custom-map-legend-"+nodeID).style;
				s.display="none";
				if($("#map-legend-title-"+nodeID).length){
					s = document.getElementById("map-legend-title-"+nodeID).style;
					s.display="none";
				}
				if (typeof indicatorAjaxRequest === 'object'){
					indicatorAjaxRequest.abort(); //if a request has been made already, but not completed. Cancel it.
				}
				if(thisMap){
					togglelayergroup('visible', nodeID);
				}
				//$('#indicator-card-tabs-'+nodeID+":visible > div:nth-of-type(1)").collapse('show');

				//console.log(e.target);
				var dataTabs =  new Set(["global-tab-" + nodeID, "regional-tab-" + nodeID, "national-tab-" + nodeID, "local-tab-" + nodeID]);
				if (dataTabs.has($(e.target).get(0).id)) {
					changeTheDataAndLayers(nodeID);
				}
			});

			var someTabTriggerEl = null;
			// var currentPath = window.location.pathname;
			if((selSettings.WDPAID !== null) && ($("#local-tab-" + nodeID + ":visible").length)){
				someTabTriggerEl = document.getElementById("local-tab-" + nodeID);
			} else if((selSettings.iso2 !== null) && ($("#national-tab-" + nodeID + ":visible").length)){
				someTabTriggerEl = document.getElementById("national-tab-" + nodeID);
			} else if((selSettings.regionID !== null) && ($("#regional-tab-" + nodeID + ":visible").length)){
				someTabTriggerEl = document.getElementById("regional-tab-" + nodeID);
			} else if ($("#global-tab-" + nodeID + ":visible").length){
				someTabTriggerEl = document.getElementById("global-tab-" + nodeID);
			}
			if (someTabTriggerEl !== null){
				var tab = new bootstrap.Tab(someTabTriggerEl);
				tab.show();
			}
			
			if(thisMap){
				addCardLayers(nodeID);
			}
			

			function changeTheDataAndLayers(nodeID){
				$( cardWrapper + " li.active" ).removeClass("active");
				$( cardWrapper + " li[aria-expanded='true']" ).addClass("active");
				indictorGlobalSettings[nodeID].chart.areaIn = '';
				if ($(cardWrapper + " a.edit-indicator.indicator-card-edit-link").length){
					$(cardWrapper + " div.edit-menu-trigger").css("display", "block");
				}
				var typeOfData = $( cardWrapper + " .field--name-field-indi-get-data-from").text();
				currentTab = $( cardWrapper +" .nav-link.active" ).text().toLowerCase().trim();
				indictorGlobalSettings[nodeID].map.mapScope = currentTab;

				setTimeout(function(){ //set timeout is needed as the tab animation can cause the chart to render wrong.
					switch(currentTab) {
						case 'global':

							if(thisMap){
								var lat = biopamaGlobal.map.center[0] + 30;
								var lon = biopamaGlobal.map.center[1];
								thisMap.flyTo({
									center: [lat, lon],
									zoom: biopamaGlobal.map.zoom,
								});
							}
							
							if ((typeOfData == "BIOPAMA Geonode") && ($( cardWrapper + " div.field--name-field-indi-geonode-global .field__item").text() == "On")){
								getGeonodeData("global", "global");
							} else {
								setupScopeData('global');
							}
							break;
						case 'regional':
							if ((typeOfData == "BIOPAMA Geonode") && ($( cardWrapper + " div.field--name-field-indi-geonode-regional .field__item").text() == "On")){
								getGeonodeData("regional", "REGION");
							} else {
								setupScopeData('regional');
							}
							break;
						case 'national':
							if ((typeOfData == "BIOPAMA Geonode") && ($( cardWrapper + " div.field--name-field-indi-geonode-country .field__item").text() == "On")){
								getGeonodeData('national', "ISO3");
							} else {
								setupScopeData('national');
							}
							break;
						case 'local':
							if ((typeOfData == "BIOPAMA Geonode") && ($( cardWrapper + " div.field--name-field-indi-geonode-pa .field__item").text() == "On")){
								getGeonodeData("local", "WDPAID");
							} else {
								setupScopeData('local');
							}
							break;
						default:
							console.log("nothing to do");
							removeCustomLayers('||a11');
							$("span.indicator-for").text("");
							$("span.indicator-country").text("");
					}
				}, 200); 

				
			}
			function getGeonodeData(scopeSelected, scopeID = null){
				indictorGlobalSettings[nodeID].map.mapScope = scopeSelected;
				indictorGlobalSettings[nodeID].chart.RESTurl = "https://dopa-services.jrc.ec.europa.eu/services/d6biopamarest/d6biopama/get_indicator_data?format=json&geonode_layer=cp84&indicator_method=area_summaries&entity_id="+ scopeID +"&children=TRUE";
				indictorGlobalSettings[nodeID].chart.mappedField = "id";
				indictorGlobalSettings[nodeID].chart.sort = "Ascending";
				indictorGlobalSettings[nodeID].chart.Yaxis = {"name":"Percent", "nameLocation": "middle", "nameGap": 40, "type":"value", "data":"percent"};
				indictorGlobalSettings[nodeID].chart.chartSeries = [{"name":"Percent", "type":"bar", "data":"percent"}];
				switch(scopeSelected) {
					case 'global':
						indictorGlobalSettings[nodeID].chart.Xaxis = {"name":"Regions", "nameLocation": "middle", "nameGap": 40, "type":"category", "data":"id"};
						indictorGlobalSettings[nodeID].chart.mapLayerField = "Group";
						break;
					case 'regional':
						indictorGlobalSettings[nodeID].chart.Xaxis = {"name":"Countries", "nameLocation": "middle", "nameGap": 40, "type":"category", "data":"id"};
						indictorGlobalSettings[nodeID].chart.mapLayerField = "iso3";
						break;
					case 'national':
						indictorGlobalSettings[nodeID].chart.Xaxis = {"name":"Protected areas", "nameLocation": "middle", "nameGap": 40, "type":"category", "data":"id"};
						indictorGlobalSettings[nodeID].chart.mapLayerField = "wdpaid";
						break;
					case 'local':
						indictorGlobalSettings[nodeID].chart.RESTurl = "https://dopa-services.jrc.ec.europa.eu/services/d6biopamarest/d6biopama/get_indicator_data?format=json&geonode_layer=cp84&indicator_method=area_summaries&entity_id="+ scopeID;
						indictorGlobalSettings[nodeID].chart.Xaxis = {"name":"Protected area", "nameLocation": "middle", "nameGap": 40, "type":"category", "data":"id"};
						indictorGlobalSettings[nodeID].chart.mapLayerField = "";
						break;
					default:
						console.log("the Geonode layer is messed up");
						removeCustomLayers('||a11');
				}
				getRestResults(nodeID);
			}
			
			function setupScopeData(scopeSelected){
				$(cardWrapper + " span.indicator-for").text(" for");
				indictorGlobalSettings[nodeID].map.mapScope = scopeSelected;
				if ($( cardWrapper + " div.field--name-field-indi-data-"+scopeSelected+" div.field--name-field-data-chart" ).length){
					//this is to see if the results have been proccessed for the chart. This will happen the first time the chart is turned on. 
					var indicatorWrapper = $(cardWrapper + " .field--name-field-indi-data-"+scopeSelected);
					
					var cardError = '';
					
					//check if there's any assigned countries to mask out the others....
					if ($( ".field--name-field-countries" ).length){
						var indicatorCountriesArray = [];
						$(indicatorWrapper).find( ".field--name-field-countries" ).children().each(function () {
							indicatorCountriesArray.push($(this).text().trim())
						});
						indictorGlobalSettings[nodeID].info.countries = indicatorCountriesArray;
					}
					
					if ($( ".field--name-field-indi-get-data-from" ).length){
						indictorGlobalSettings[nodeID].chart.dataSource = $(indicatorWrapper).find( ".field--name-field-indi-get-data-from.field__item" ).text();
					} else {
						indictorGlobalSettings[nodeID].chart.dataSource = "";
						cardError = cardError + "No data Source was Provided<br>"
					}

					//for each of these we work our way up from the active url. then find the field we need. This is due to the way drupal views generates the table
					if ($( ".field--name-field-data-rest-url" ).length){
						var CTTrestURL = $(indicatorWrapper).find( ".field--name-field-data-rest-url.field__item" ).text();
						indictorGlobalSettings[nodeID].chart.RESTurl = CTTrestURL;
						if ((CTTrestURL.indexOf('ISO2') >= 0) || (CTTrestURL.indexOf('ISO3') >= 0) || (CTTrestURL.indexOf('NUM') >= 0)) indictorGlobalSettings[nodeID].chart.RESTfilter = 'Country';
						if (CTTrestURL.indexOf("Group") >= 0) indictorGlobalSettings[nodeID].chart.RESTfilter = 'Region';
						if (CTTrestURL.indexOf("WDPAID") >= 0) indictorGlobalSettings[nodeID].chart.RESTfilter = 'PA';
					} else {
						indictorGlobalSettings[nodeID].chart.RESTurl = "";
						cardError = cardError + "No REST URL has been defined<br>"
					}
					if ($( ".field--name-field-data-country" ).length){
						var dataCountriesArray = [];
						$(indicatorWrapper).find( ".field--name-field-data-country" ).children().each(function () {
							dataCountriesArray.push($(this).text().trim())
						});
						indictorGlobalSettings[nodeID].data.countries = dataCountriesArray;
					}
					if ($( ".field--name-field-rest-field-context" ).length){
						indictorGlobalSettings[nodeID].chart.RESTdataContext = $(indicatorWrapper).find( ".field--name-field-rest-field-context.field__item" ).text();
					} else {
						indictorGlobalSettings[nodeID].chart.RESTdataContext = "";
						cardError = cardError + "No REST Context was Provided<br>"
					}
					if ($( ".field--name-field-data-map-attribute-link" ).length){
						indictorGlobalSettings[nodeID].chart.mapLayerField = $(indicatorWrapper).find( ".field--name-field-data-map-attribute-link.field__item" ).text();
						if (indictorGlobalSettings[nodeID].chart.mapLayerField == "WDPAID") indictorGlobalSettings[nodeID].chart.mapLayerField = "wdpaid";
					}
					if ($( ".field--name-field-data-rest-map-field-link" ).length){
						indictorGlobalSettings[nodeID].chart.mappedField = $(indicatorWrapper).find( ".field--name-field-data-rest-map-field-link.field__item" ).text();
					}
					if ($( ".field--name-field-data-map-attribute-link" ).length){
						indictorGlobalSettings[nodeID].chart.mapPoints = $(indicatorWrapper).find( ".field--name-field-indicator-make-map-points.field__item" ).text().trim();
					}
					if ($( ".chart-xaxis" ).length){
						var indicatorXaxis = $(indicatorWrapper).find( ".chart-xaxis" ).text().trim();
						try {
							indictorGlobalSettings[nodeID].chart.Xaxis = JSON.parse(indicatorXaxis);
						} catch (e) {
							cardError = cardError + "The X Axis is not setup correctly<br>";
							indictorGlobalSettings[nodeID].chart.Xaxis = '{}';
						}
					}
					if ($( ".chart-yaxis" ).length){
						var indicatorYaxis = $(indicatorWrapper).find( ".chart-yaxis" ).text().trim();
						try {
							indictorGlobalSettings[nodeID].chart.Yaxis = JSON.parse(indicatorYaxis);
						} catch (e) {
							cardError = cardError + "The Y Axis is not setup correctly<br>";
							indictorGlobalSettings[nodeID].chart.Yaxis = '{}';
						}			
					}
					if ($( ".chart-series" ).length){
						var indicatorSeries = $(indicatorWrapper).find( ".chart-series" ).text().trim();
						indicatorSeries = indicatorSeries.replace(/(}\s*\s)+/g, "}||").split("||");
						indicatorSeries.forEach(function(object, index){
							try {
								indicatorSeries[index] = JSON.parse(object);
							} catch (e) {
								cardError = cardError + "Series "+ index +" is not setup correctly<br>";
								indicatorSeries = '{}';
							}	
						});		
						indictorGlobalSettings[nodeID].chart.chartSeries = indicatorSeries;
					}
					if ($( ".field--name-field-indicator-or-ranking" ).length){
						indictorGlobalSettings[nodeID].chart.ranking = $(indicatorWrapper).find( ".field--name-field-indicator-or-ranking.field__item" ).text().trim()
					}
					if ($( ".field--name-field-data-color-inverse" ).length){
						if($(indicatorWrapper).find( ".field--name-field-data-color-inverse.field__item" ).text().trim() == "Yes"){
							indictorGlobalSettings[nodeID].customColorInvert = true;
						}
					}
					if ($( ".field--name-field-chart-bl-sort" ).length){
						indictorGlobalSettings[nodeID].chart.sort = $(indicatorWrapper).find( ".field--name-field-chart-bl-sort.field__item" ).text().trim();
					}
					if ($( ".chart-pie-type" ).length){
						indictorGlobalSettings[nodeID].chart.pie.type = $(indicatorWrapper).find( ".chart-pie-type" ).text().trim();
					}
					if ($( ".chart-pie-rest-limit" ).length){
						indictorGlobalSettings[nodeID].chart.pie.limit = $(indicatorWrapper).find( ".chart-pie-rest-limit" ).text().trim();
					}
					if ($( ".chart-pie-over-limit" ).length){
						indictorGlobalSettings[nodeID].chart.pie.limitOverLabel = $(indicatorWrapper).find( ".chart-pie-over-limit" ).text().trim();
					}
					if ($( ".chart-pie-under-limit" ).length){
						indictorGlobalSettings[nodeID].chart.pie.limitUnderLabel = $(indicatorWrapper).find( ".chart-pie-under-limit" ).text().trim();
					}
					if ($( ".chart-pie-unit" ).length){
						indictorGlobalSettings[nodeID].chart.pie.unit = $(indicatorWrapper).find( ".chart-pie-unit" ).text().trim();
					}
					if ($( ".field--name-field-data-classes" ).length){
						indictorGlobalSettings[nodeID].customNumClasses = $(indicatorWrapper).find( ".field--name-field-data-classes.field__item" ).text().trim();
					}
					if ($( ".field--name-field-data-classification-method" ).length){
						indictorGlobalSettings[nodeID].customClassMethod = $(indicatorWrapper).find( ".field--name-field-data-classification-method.field__item" ).text().trim();
					}
					if ($( ".field--name-field-data-color-swatch" ).length){
						indictorGlobalSettings[nodeID].mapCustomSwatchColors = $(indicatorWrapper).find( ".field--name-field-data-color-swatch.field__item" ).text().trim();
						var swatch = indictorGlobalSettings[nodeID].mapCustomSwatchColors;
						var tempCustomSwatch = swatch.split("-");
						indictorGlobalSettings[nodeID].mapCustomSwatchCat = tempCustomSwatch[0]
						indictorGlobalSettings[nodeID].mapCustomSwatchNum = parseInt(tempCustomSwatch[1], 10)
					}
					if ($( ".field--name-field-data-area-not-in" ).length){
						indictorGlobalSettings[nodeID].chart.areaIn = $(indicatorWrapper).find( ".field--name-field-data-area-not-in.field__item" ).text().trim();
					} else {
						indictorGlobalSettings[nodeID].chart.areaIn = '';
					}
					if (cardError != '') console.log(cardError);

					if(thisMap){
						addTabLayers(scopeSelected, nodeID);
					}
					
					switch(scopeSelected) {
						case 'global':
							if(thisMap){
								var lat = biopamaGlobal.map.center[0] + 30;
								var lon = biopamaGlobal.map.center[1];
								thisMap.flyTo({
									center: [lat, lon],
									zoom: biopamaGlobal.map.zoom,
								});
							}
							getRestResults(nodeID);
							break;
						case 'regional':
							if(selSettings.regionName){
								if(thisMap){
									$().mapZoomToRegion(thisMap, selSettings.regionName);
								}
								getRestResults(nodeID);
							} else {
								indicatorChartError(2, nodeID);
							}
							break;
						case 'national':
							if(selSettings.iso2){
								if(thisMap){
									$().mapZoomToCountryIso2(thisMap, selSettings.iso2);
								}
								getRestResults(nodeID);
							} else {
								indicatorChartError(2, nodeID);
							}
							break;
						case 'local':
							if(selSettings.WDPAID){
								if(thisMap){
									$().mapZoomToPA(thisMap, selSettings.WDPAID);
								}
								getRestResults(nodeID);
							} else {
								indicatorChartError(2, nodeID);
							}
							break;
						default:
							console.log("nothing to do");
					}
				} else {
					getChart(0, nodeID);
				}
			}

			});
		}
	};
})(jQuery, Drupal);

function checkForLayersToRemove(nodeID = ""){
	//console.log(jQuery(".indi-tab-country.ui-tabs-active").length);
	if (jQuery(".indi-tab-global.active").length === 0) {
		removeCustomLayers('-gl0b4l'+nodeID);
	}
	if (jQuery(".indi-tab-regional.active").length === 0){
		removeCustomLayers('-r3g10n'+nodeID);
	}
	if (jQuery(".indi-tab-national.active").length === 0){
		removeCustomLayers('-n4t10n41'+nodeID);
	}
	if (jQuery(".indi-tab-local.active").length === 0){
		removeCustomLayers('-l0c4l'+nodeID);
	}
}
function addCardLayers(nodeID = ""){
	var $ = jQuery;
	var cardWrapper;
	var tabLayerKey = '-b10p4m4'+nodeID;
	if (nodeID != ""){
		cardWrapper = "#id-"+nodeID;
	}
	
	checkForLayersToRemove(nodeID);
	if (jQuery(cardWrapper + " .field--name-field-indi-map-layers-all" ).length){
		//I change the map layer loading to be different
		var mapLayer = {};

		var layerNid = $(cardWrapper + " .layer_nid").text().trim();

		$(cardWrapper + " .field--name-field-indi-map-layers-all .field__items" ).children().each(function () {
			var proxy = "https://proxy.biopama.org/cors/";
			mapLayer.name = $(this).find(".map-layer-name").text().trim();
			mapLayer.id = $(this).find(".map-layer-id").text().trim();
			mapLayer.legend = $(this).find(".map-legend").text().trim();
			mapLayer.DataType = $(this).find(".map-layer-data-type").text().trim().toLowerCase();
			mapLayer.ServerType = $(this).find(".map-layer-server-type").text().trim();
			mapLayer.baseWMS = $(this).find(".map-layer-wms").text().trim();
			mapLayer.WMStime = $(this).find(".map-layer-wms-time").text().trim();
			mapLayer.WMSstyle = $(this).find(".map-layer-wms-style").text().trim();
			mapLayer.json = $(this).find(".map-layer").text();
			mapLayer.json = JSON.parse(mapLayer.json.replace("'", "\""));
			mapLayer.uid = mapLayer.json.id + tabLayerKey;
			var  year_ts, month_ts, day_ts;
			
			if($("#timeseries-wrapper-" + layerNid).length >= 1 ){
				year_ts = ($("#timeseries-wrapper-" + layerNid + " .drop_year_layer").val());
				month_ts = ($("#timeseries-wrapper-" + layerNid + " .drop_month_layer").val());
				day_ts = ($("#timeseries-wrapper-" + layerNid + " .drop_day_layer").val());
			}

			if (mapLayer.DataType == "tms"){
				mapLayer.ServerType = "tms";
			}
			if (mapLayer.DataType == "xyz"){
				mapLayer.ServerType = "xyz";
			}

			switch (mapLayer.ServerType) {
				case 'GeoWebCache':
				case 'tms':
				case 'xyz':
					var wmsURL = mapLayer.json.source.tiles[0];
					var tiles = "";
					if (wmsURL.indexOf("geoserver") > -1){
						var wmsBase = /.*?(?=\/geoserver)/.exec(wmsURL)[0]; 
						tiles = wmsBase + '/geoserver/gwc/service/tms/1.0.0/' + mapLayer.json.id + '@EPSG:900913@png/{z}/{x}/{y}.png'
					} else {
						tiles = mapLayer.baseWMS;
					}
					var scheme = "tms"
					if (mapLayer.ServerType == "xyz"){
						scheme = "xyz"
					}

					thisMap.addLayer({
						'id': mapLayer.uid,
						'type': "raster",
						'source': {
						  'type': "raster",
						  'tiles': [tiles],
						  'tileSize': 256,
						  'scheme': scheme,
						},
						'paint': {}
					  });
					  $().pushTopMapLayers(thisMap);
				  	break;
				case 'ESRItiles':
				case 'UNLAB':
				case 'gbif':
					thisMap.addLayer({
						'id':  mapLayer.uid,
						'type': 'raster',
						'source': {
							'type': 'raster',
							'tiles': [mapLayer.baseWMS],
							'tileSize': 256,
						},
						'paint': {}
					});
					$().pushTopMapLayers(thisMap);
					break;
				case 'ESRI':
					mapLayer.baseWMS = mapLayer.baseWMS.replace("https://", proxy)
					thisMap.addLayer({
						'id':  mapLayer.uid,
						'type': 'raster',
						'source': {
							'type': 'raster',
							'tiles': [mapLayer.baseWMS + "?" +
								"token=" + "ZKRjsVzCgYCalBO_-aSIyFJc2ukLchzFoJb_7VcMYjsyQKlGA0reYZHJxWkUykmH8UfnLDqqlOtATQ9-zKxwLo5efjE_HWerTq3JFq7GpeosvgjRVXGmqamKFhSlb7hL0D9I2gt8d2sksUCPo_xsAyGwW1re5hOgFg7VkNt1pcsqMSuyAu58I7B5uHug5JW6KK4ppaEn3LIZLQmKEoS85x-6djnrLTTbDbb8l4zlbhw."
							],
							'tileSize': 256,
						},
						'paint': {}
					});
					$().pushTopMapLayers(thisMap);
					break;
				case 'coralreefwatch':
					thisMap.addLayer({
						'id': mapLayer.uid,
						'type': 'raster',
						'source': {
							'type': 'raster',
							'tiles': [ proxy + 'coralreefwatch.noaa.gov/data1/vs/google_maps/' +
								mapLayer.id + '/{z}/{x}/{y}.png?' +
								"&transparent=true" +
								"&bbox={bbox-epsg-3857}"
							],
							// 'tiles': ['https://coralreefwatch.noaa.gov/data1/vs/google_maps/' +
							// 	mapLayer.id + '/{z}/{x}/{y}.png?' +
							// 	"&transparent=true" +
							// 	"&bbox={bbox-epsg-3857}"
							// ],
							'tileSize': 256,
							'scheme': "tms",
						},
						'paint': {}
					});
					$().pushTopMapLayers(thisMap);
					break;
				case 'timeseries':
					if (mapLayer.baseWMS.indexOf("?") == -1){
						mapLayer.baseWMS = mapLayer.baseWMS + "?";
					} 
					console.log(mapLayer.WMStime);
					if (mapLayer.WMStime){
						console.log("test");
					}
					thisMap.addLayer({
						'id': mapLayer.uid,
						'type': 'raster',
						'source': {
							'type': 'raster',
							'tiles': [mapLayer.baseWMS + 
								"service=WMS&request=GetMap&width=256&height=256" +
								"&layers=" + mapLayer.id +
								"&srs=EPSG:3857" +
								"&format=image/png" +
								"&styles=" + mapLayer.WMSstyle +
								"&time=" + year_ts + "-" + month_ts + "-" + day_ts + 
								"&transparent=true" +
								"&version=1.1.1" +
								"&bbox={bbox-epsg-3857}"
							],
							'tileSize': 256,
						},
						'paint': {}
					});
					$().pushTopMapLayers(thisMap);
					break;
				case 'Edo':
					if (mapLayer.baseWMS.indexOf("?") == -1){
						mapLayer.baseWMS = mapLayer.baseWMS.replace("https://", proxy + "https://");
						mapLayer.baseWMS = mapLayer.baseWMS + "?";
					} else{
						mapLayer.baseWMS = mapLayer.baseWMS + "&";
					}
					thisMap.addLayer({
						'id':  mapLayer.uid,
						'type': 'raster',
						'source': {
							'type': 'raster',
							'tiles': [mapLayer.baseWMS + 
								"SERVICE=WMS&REQUEST=GetMap&WIDTH=256&HEIGHT=256" +
								"&LAYERS=" + mapLayer.id +
								"&TRANSPARENT=true" +
								"&TILED=true" +
								"&FORMAT=image/png" +
								"&SRS=EPSG:3857" +
								"&CRS=EPSG:3857" +
								"&SELECTED_YEAR=" + year_ts +
								"&SELECTED_MONTH=" + month_ts +
								"&SELECTED_TENDAYS=" + day_ts +
								"&TIME=" + year_ts + "-" + month_ts + "-" + day_ts +
								"&VERSION=1.1.1" +
								"&BBOX={bbox-epsg-3857}"
							],
							'tileSize': 256,
						},
						'paint': {}
					});
					$().pushTopMapLayers(thisMap);
					break;
				case 'Floods':
					var today_floods = new Date(); //floods
					var dd_floods = today_floods.getDate();
					var mm_floods = today_floods.getMonth() + 1;
					var yyyy_floods = today_floods.getFullYear();
					if (mm_floods < 10) { mm_floods = '0' + mm_floods }
					today_floods = yyyy_floods + '-' + mm_floods + '-' + dd_floods;
	
					mapLayer.baseWMS = mapLayer.baseWMS.replace("https://", proxy)
					thisMap.addLayer({
						'id':  mapLayer.uid,
						'type': 'raster',
						'source': {
							'type': 'raster',
							'tiles': [mapLayer.baseWMS + "?" + 
								"service=WMS&request=GetMap&width=256&height=256" +
								"&layers=" + mapLayer.id +
								"&transparent=true" +
								"&format=image/png" +
								"&crs=EPSG:3857" +
								"&MAP_RESOLUTION=180" +
								"&time=" + today_floods + 
								"&styles=" + mapLayer.WMSstyle +
								"&version=1.3.0" +
								"&bbox={bbox-epsg-3857}"
							],
							'tileSize': 256,
						},
						'paint': {}
					});
					$().pushTopMapLayers(thisMap);
					break;
				case 'Python':
					console.log(mapLayer);
					thisMap.addLayer({
						'id':  mapLayer.uid,
						'type': 'raster',
						'source': {
							'type': 'raster',
							'tiles': [mapLayer.baseWMS + 
								"service=WMS&request=GetMap&width=256&height=256" +
								"&layers=" + mapLayer.id +
								"&transparent=true" +
								"&format=image/png" +
								"&crs=EPSG:3857" +
								"&time=2010" + 
								"&styles=" + mapLayer.WMSstyle +
								"&version=1.3.0" +
								"&bbox={bbox-epsg-3857}"
							],
							'tileSize': 256,
						},
						'paint': {}
					});
					$().pushTopMapLayers(thisMap);
					break;
				case 'Google':
					thisMap.addLayer({
						'id':  mapLayer.uid,
						'type': 'raster',
						'source': {
							'type': 'raster',
							'tiles': ['https://storage.googleapis.com/' + 
								mapLayer.id + '/{z}/{x}/{y}.png?'
							],
							'tileSize': 256,
							'scheme': "xyz",
						},
						'paint': {}
					});
					$().pushTopMapLayers(thisMap);
					break;
				case 'spi':
					var today_edo = new Date(); //drought
					var dd_edo = today_edo.getDate();
					var mm_edo =  (today_edo.getMonth()); //January is 0!
					var yyyy_edo = today_edo.getFullYear();
					if (dd_edo <= 10) { dd_edo = '01' } else if (dd_edo >= 11 && dd_edo <= 19) { dd_edo = '11' } else if (dd_edo >= 20) { dd_edo = '21' } else {}
					if (mm_edo < 10) { mm_edo = '0' + mm_edo }
					if (mm_edo == 0) { mm_edo = 12 }
					today_edo = yyyy_edo + '-' + (mm_edo) + '-' + dd_edo;
					var mm_spi = mm_edo - 1
					var mm_edo = mm_edo 
					today_spi = yyyy_edo + '-' + '0' + mm_spi;
	
					mapLayer.baseWMS = mapLayer.baseWMS.replace("https://", proxy)
					thisMap.addLayer({
						'id':  mapLayer.uid,
						'type': 'raster',
						'source': {
							'type': 'raster',
							'tiles': [mapLayer.baseWMS + "?" + 
								"service=WMS&request=GetMap&width=256&height=256" +
								"&layers=" + mapLayer.id +
								"&transparent=true" +
								"&format=image/png" +
								"&crs=EPSG:3857" +
								"&srs=EPSG:3857" +
								"&MAP_RESOLUTION=180" +
								"&time=" + today_spi + 
								"&SELECTED_TIMESCALE=03" +
								"&version=1.1.1" +
								"&bbox={bbox-epsg-3857}"
							],
							'tileSize': 256,
						},
						'paint': {}
					});
					$().pushTopMapLayers(thisMap);
					break;
				case 'Geoserver':
				case '':
				default:
					try {
						mapLayer.json.id = mapLayer.uid;
						// mapLayer.json.source.type = mapLayer.DataType;
						// mapLayer.json.type = mapLayer.DataType;

						// if (mapLayer.DataType == "vector"){
						// 	mapLayer.json.source.tileSize = 512;
						// }
		
						//WMS URL Cleaning! Thanks to AKP data import
						var LayerURL = mapLayer.json.source.tiles[0];
						mapLayer.json.source.tiles[0] = cleanWMSURL(LayerURL);
						function cleanWMSURL(LayerURL){
							if (LayerURL.indexOf("eosdis.nasa.gov") >= 1){//we assume anything with "/wms?" is correct
								LayerURL = mapLayer.baseWMS + "&layers=" + mapLayer.id + "&format=image/png&transparent=true&SERVICE=WMS&VERSION=1.1.1&HEIGHT=256&WIDTH=256&SRS=EPSG:3857&bbox={bbox-epsg-3857}";
								return LayerURL;
							}
							var wmsCheck = LayerURL.slice(-4);
							if (wmsCheck == "/wms"){ //it has "/wms" but no "?" at the end of the string
								LayerURL = LayerURL.replace("/wms", "/wms?") //add "?"
								return LayerURL; //we end it as anything with WMS already has the workspace added
							}
							if (LayerURL.indexOf("/wms?") == -1){//we assume anything without "/wms?" is correct
								
								//get the layer name
								var layerName = /layers=([^&]+)/.exec(LayerURL)[1]; 
								if (layerName.indexOf(":") > -1){
									var layerParts = layerName.split(':');
									var workspace = layerParts[0];
									if ((LayerURL.indexOf("/geoserver/"+workspace) == -1)){
										LayerURL = LayerURL.replace("/geoserver", "/geoserver/"+workspace+"/wms?")
									} 
									if (LayerURL.indexOf("/wms?") == -1){
										LayerURL = LayerURL.replace("/geoserver/"+workspace, "/geoserver/"+workspace+"/wms?")
									}
								}
								
								if (LayerURL.indexOf("estation.jrc.ec.europa.eu") > -1) {
									mapLayer.json = {
										'id':  mapLayer.uid,
										'type': 'raster',
										'source': {
											'type': 'raster',
											'tiles': [
											'https://estation.jrc.ec.europa.eu/eStation3/webservices?SERVICE=WMS&&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image/png'+
											'&LAYERS=' + mapLayer.id + 
											'&TRANSPARENT=true&WIDTH=256&HEIGHT=256&CRS=EPSG:3857&BBOX={bbox-epsg-3857}'
											],
											'tileSize': 256,
										},
										'paint': {}
									};
								}
							}
							return LayerURL;
						}

						//we need to know if the current source has already been added, and if so make sure it's referenced rather then added again.
						var currentSources = thisMap.style.sourceCaches
						for (var key in currentSources) {
							if (currentSources[key].id == mapLayer.json.source) {
								mapLayer.json.source = currentSources[key].id;
							} 
						}
						thisMap.addLayer(mapLayer.json); 
						$().pushTopMapLayers(thisMap);
						jQuery(cardWrapper + " #map-legend-wrapper-"+nodeID).append("<div class='"+mapLayer.id+"'><div class='map-legend-title'>"+mapLayer.name+"</div><img src="+mapLayer.legend+"></div>");
					} catch (e) {
						
						console.log("Map layer error");
						console.log(e);
						mapLayer.json = '{}'; 
					}
			}

			if ($(cardWrapper + " .layer_nid").length) {
				const layerHideTrigger = document.getElementById('layerHide'+nodeID);
				var tooltip = bootstrap.Tooltip.getInstance("#layerHide"+nodeID);
				if (layerHideTrigger) {
					$( '#layerHide'+nodeID ).unbind(); //incase the event was already bound before a timeseries select change
					$( '#layerHide'+nodeID ).on( "click touch", function() {
						var targetClasses = $("#layerHide"+nodeID + " i").attr("class");;
						if (targetClasses.indexOf('fa-eye-slash') >= 1 ) {
							$().setCardLayersOpactity(thisMap, nodeID, 0);	
							$(cardWrapper + " .fa-eye-slash").removeClass("fa-eye-slash").addClass("fa-eye");
                    		tooltip.setContent({ '.tooltip-inner': 'Show' });
							document.getElementById("layerSlider"+nodeID).value = "0";
						} else {
							$().setCardLayersOpactity(thisMap, nodeID, 1);	
							$(cardWrapper + " .fa-eye").removeClass("fa-eye").addClass("fa-eye-slash");
                    		tooltip.setContent({ '.tooltip-inner': 'Hide' });
							document.getElementById("layerSlider"+nodeID).value = "100";
						}
					})
					var slider = document.getElementById("layerSlider"+nodeID);
					slider.addEventListener('input', function (e) {
						var opacityVal  = parseInt(e.target.value, 10) / 100;
						//thisMap.setPaintProperty( mapLayer.uid, 'raster-opacity', parseInt(e.target.value, 10) / 100 );
						$().setCardLayersOpactity(thisMap, nodeID, opacityVal);	
						if (opacityVal > 0.01){
							$(".fa-eye").removeClass("fa-eye").addClass("fa-eye-slash");
                    		tooltip.setContent({ '.tooltip-inner': 'Hide' });
						} else {
							$(".fa-eye-slash").removeClass("fa-eye-slash").addClass("fa-eye");
                    		tooltip.setContent({ '.tooltip-inner': 'Show' });
						}
					});
				}

				$("#timeseries-wrapper-" + layerNid + " .drop_year_layer, #timeseries-wrapper-" + layerNid + " .drop_month_layer, #timeseries-wrapper-" + layerNid + " .drop_day_layer ").on('change', function() {
					var  year_ts = ($("#timeseries-wrapper-" + layerNid + " .drop_year_layer").val());
					var  month_ts = ($("#timeseries-wrapper-" + layerNid + " .drop_month_layer").val());
					var  day_ts = ($("#timeseries-wrapper-" + layerNid + " .drop_day_layer").val());

					//console.log(day_ts + "/" + month_ts + "/" + year_ts);
					removeCustomLayers(nodeID);
					addCardLayers(nodeID);
				});
			}
		});
	}
}

function addTabLayers(tab, nodeID = ""){
	var tabLayerKey;
	checkForLayersToRemove();
	if (jQuery( "#id-"+nodeID+" div.field--name-field-indi-data-"+tab+" div.field--name-field-data-map-layers" ).length){
		//console.log("found")
		switch(tab) {
			case 'global':
				tabLayerKey = '-gl0b4l'+nodeID+'||a11';
				break;
			case 'regional':
				tabLayerKey = '-r3g10n'+nodeID+'||a11';
				break;
			case 'national':
				tabLayerKey = '-n4t10n41'+nodeID+'||a11';
				break;
			case 'local':
				tabLayerKey = '-l0c4l'+nodeID+'||a11';
				break;
			default:
				tabLayerKey = null;
		}
		if (tabLayerKey != null){
			//I change the map layer loading to be different
			var mapLayersArray = [];
			var mapLayer;
			var mapLayerName = "";
			var mapLegend;
			jQuery( "#id-"+nodeID+" div.field--name-field-indi-data-"+tab+" .field--name-field-data-map-layers.field__items" ).children().each(function () {
				mapLayerName = jQuery(this).find(".map-layer-name").text();
				mapLayer = jQuery(this).find(".map-layer").text();
				mapLegend = jQuery(this).find(".map-legend").text();
				try {
					mapLayer = JSON.parse(mapLayer.replace("'", "\""))
					mapLayer.id = mapLayer.id + tabLayerKey;
					//we need to know if the current source has already been added, and if so make sure it's referenced rather then added again.
					var currentSources = thisMap.style.sourceCaches
					for (var key in currentSources) {
						if (currentSources[key].id == mapLayer.source) {
							mapLayer.source = currentSources[key].id;
						} 
					}
					thisMap.addLayer(mapLayer, 'gaulACP'); 

					jQuery("#id-"+nodeID+" #wms-map-legend").append("<div class='"+mapLayer.id+"'><div class='map-legend-title'>"+mapLayerName+"</div><img src="+mapLegend+"></div>");
				} catch (e) {
					console.log("You have a messed up layer in the "+tab+" tab")
					mapLayer = '{}'; 
				}	
				//console.log(thisMap.style._layers)
			});
		}
	}
}


