
//initiates the chart building
function getChart(error = 0, nodeID = 0){
	if (error == 0 ){
		if (nodeID == 0){
			console.log("No indicator card node ID found for the chart!");
			return;
		}
		var cardWrapper = "#id-"+ nodeID;
		//delete any old errors now that we have success
		jQuery( ".rest-error" ).empty();
		jQuery( ".indicator-chart" ).show();
		currentCardScope = jQuery( cardWrapper +" .nav-link.active" ).text().toLowerCase().trim();
		//destroy any active charts, this is to remove any active events that might be attached to them, it also looks nicer with the animations being refreshed.
		//echarts.dispose(document.getElementById('indicator-chart-'+CurrentCardScope));
		indicatorCharts[nodeID] = echarts.init(document.getElementById("indicator-chart-" + currentCardScope + "-" + nodeID));
		//if there's a chart already unbind all the events attached to it. 
		//this stops the events being carried over from other countries, making the chart navigation buggy.
		//the events will be readded as needed below
		if (indicatorCharts[nodeID].id != null){
			indicatorCharts[nodeID].off();
		}
		//indicatorCharts[nodeID].disconnect();
		//initalize the chart library and attach it to the correct div
		//show the chart, in case it was hidden from a previous error
		
		if ((indictorGlobalSettings[nodeID].chart['RESTResults']) && (indictorGlobalSettings[nodeID].chart.areaIn !== '')){
			switch(indictorGlobalSettings[nodeID].chart.areaIn) {
			case "1":
				jQuery( cardWrapper +" .rest-error" ).html("<div class='alert alert-info'>No data is available for the selected area.</div>");
				jQuery( cardWrapper +" .indicator-chart" ).hide();
				break;
			case "2":
				jQuery( cardWrapper +" .indicator-chart" ).hide();
				break;
			default:	//the country charts are the default for now....
				break;
			}
			//return;
		};
		//field--name-field-indi-data-global
		jQuery( "#id-"+nodeID+" .field--name-field-indi-data-"+currentCardScope+" .field--name-field-data-chart .chart-title").insertBefore('#indicator-chart-'+currentCardScope+'-'+nodeID);
		jQuery( "#id-"+nodeID+" .field--name-field-indi-data-"+currentCardScope+" .field--name-field-data-chart .chart-description").insertAfter('#indicator-chart-'+currentCardScope+'-'+nodeID);
		
		// var thisTitle = jQuery( "#id-"+nodeID+" .field--name-field-indi-data-"+currentCardScope+" .field--name-field-data-chart .chart-title").text().trim();
		// var thisDescription = jQuery( "#id-"+nodeID+" .field--name-field-indi-data-"+currentCardScope+" .field--name-field-data-chart .chart-description").html();
		// jQuery( thisTitle ).insertBefore('#indicator-chart-'+currentCardScope+'-'+nodeID);
		// jQuery( thisDescription ).insertAfter('#indicator-chart-'+currentCardScope+'-'+nodeID);

		switch(currentCardScope) {
			case "global":
				prepGlobalChart(nodeID);
				break;
			case "regional":
				prepRegionalChart(nodeID);
				break;
			case "national":
				prepCountryChart(nodeID);
				break;
			case "local":
				prepPaChart(nodeID);
				break;
			default:	//the country charts are the default for now....
				noChart(nodeID);
		}
	} else {
		indicatorChartError(error, nodeID)
	}	
}

function indicatorChartError(error, nodeID){
	//we hide the chart because it has a fixed size, and if it's empty (from the error) it takes too much space and looks funny.
	jQuery( "#id-"+ nodeID +" .indicator-chart" ).hide();
	if(error == 2){
		var scopeNeeded = '';
		switch(indictorGlobalSettings[nodeID].map.mapScope){
			case 'local':
				scopeNeeded = "protected area";	
				break;
			case 'regional':
				scopeNeeded = "region";
				break;
			case 'national':
				scopeNeeded = "country";
				break;
			default:
				scopeNeeded = "area";
				console.log("scope error");
				break;
		}
		var currentScope = jQuery("ul.indicator-card-tabs-").children(".active").first().text();

		if(((selSettings.regionName != null) && (currentScope == "Regional")) || ((selSettings.iso2 != null) && (currentScope == "National")) || ((selSettings.WDPAID > 0) && (currentScope == "Local"))){
			jQuery( ".rest-error" ).html("<div class='alert alert-info'>No results were found.<br>"+
			"Try selecting a different <b>" + scopeNeeded + "</b></div>");
			attachScopeSelector(scopeNeeded, nodeID);
		} else {
			jQuery( ".rest-error" ).html("<div class='alert alert-info'>You must select a <b>" + scopeNeeded + "</b></div>");
			attachScopeSelector(scopeNeeded, nodeID);
		}
	} else if (error == 3) {
		jQuery( ".rest-error" ).html("<div class='alert alert-warning'>There is no data for this country.</div>");
	} else {
		jQuery( ".rest-error" ).html("<div class='alert alert-warning'>There was an error.</div>");
	}
}
function attachScopeSelector(scopeNeeded, nodeID){
	
	if(scopeNeeded == "region"){
		var regionalSelect = jQuery().createRegionalSelect();
		jQuery("#id-"+nodeID+" .rest-error").append( regionalSelect );
		jQuery('.biopama-region-selector').on('change', function() {
			selSettings.regionID = jQuery(this).val();
			selSettings.regionName = jQuery("#region-selector:visible > option:selected").text();
			jQuery().mapZoomToRegion(thisMap, selSettings.regionID);
			getRestResults(nodeID); 
		});
	} else if (scopeNeeded == "country"){
		var nationalSelect = jQuery().createNationalSelect();
		jQuery("#id-"+nodeID+" .rest-error").append( nationalSelect );
		jQuery('.biopama-country-selector').on('change', function(e) {
			console.log(selSettings.iso2);
			var tempIso2 = jQuery(this).val();
			if (selSettings.iso2 !== tempIso2){
				countryChanged = 1;
				selSettings.iso2 = tempIso2;
				jQuery().mapZoomToCountryIso2(thisMap, selSettings.iso2);
			}
		});
	} else {
		return;
	}
}

function sortByKey(array, key, order='asc') {
    return array.sort(function(a, b) {

		if(!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
		  // property doesn't exist on either object
			return 0; 
		}
		var varA = parseFloat(a[key]);
		if (isNaN(varA)) varA = 0;
		var varB = parseFloat(b[key]);
		if (isNaN(varB)) varB = 0;
		//const varA = (typeof a[key] === 'string') ? a[key].toUpperCase() : a[key];
		//const varB = (typeof b[key] === 'string') ? b[key].toUpperCase() : b[key];

		//console.log(parseFloat(varA) + "  " + parseFloat(varB))
		let comparison = 0; //if equal no change
		if (varA > varB) {
		  comparison = 1;
		} else if (varA < varB) {
		  comparison = -1;
		}
		//console.log(comparison)
		return (
		  (order == 'desc') ? (comparison * -1) : comparison
		);
	});
}

function checkChartData(chartType = "barLine", nodeID){
	if (!chartSettings[nodeID]){
		chartSettings[nodeID] = {};
	}
	//we take the REST results and put it into a new variable, this ensures the original results are stored unmodified.
	var indicatorResults = indictorGlobalSettings[nodeID].chart.RESTResults;

	if ((indicatorResults == null) || (indicatorResults == "null")) {
		indicatorChartError(3, nodeID);
		return;
	}
	var countries = drupalSettings.countries;
	//we need to see if the data was from a REST service or a WMS service as the WMS service needs to be formatted differently
	if (indictorGlobalSettings[nodeID].chart.dataSource == "External Map Data" || indictorGlobalSettings[nodeID].chart.RESTdataContext == "features"){
		var wmsData = [];
		indicatorResults.forEach(function(object){
			wmsData.push(object['properties']);
		});
		//console.log(wmsData);
		indicatorResults = wmsData
	}
	//if this is a country linked visualisation, lets clean the results to ensure only ACP countries are showing in the chart.
	if (indictorGlobalSettings[nodeID].chart.mapField  == 'un_m49') {
		indicatorResults = indicatorResults.filter(ACPNUMFilter);
	}
	if (indictorGlobalSettings[nodeID].chart.mapLayerField  == 'iso3') {
		indicatorResults = indicatorResults.filter(ACPISO3Filter);
	}
	if (indictorGlobalSettings[nodeID].chart.mapLayerField  == 'iso2') {
		indicatorResults = indicatorResults.filter(ACPISO2Filter);
	}
	function ACPISO2Filter(elem, index, array) {
		const i = countries.findIndex(e => e.iso2 === elem[indictorGlobalSettings[nodeID].chart.mappedField]);
		if (i > -1) {
			elem['countryLabel'] = countries[i].name;
			elem['countryISO2'] = countries[i].iso2;
			return true;
		} else {
			//console.log("Country code: "+elem[indictorGlobalSettings[nodeID].chart.mappedField]+" is not in the ACP");
			return false; 
		}
	}
	function ACPISO3Filter(elem, index, array) {
		const i = countries.findIndex(e => e.iso3 === elem[indictorGlobalSettings[nodeID].chart.mappedField]);
		if (i > -1) {
			elem['countryLabel'] = countries[i].name;
			elem['countryISO2'] = countries[i].iso2;
			return true;
		} else {
			//console.log("Country code: "+elem[indictorGlobalSettings[nodeID].chart.mappedField]+" is not in the ACP");
			return false; 
		}
	}
	function ACPNUMFilter(elem, index, array) {
		const i = countries.findIndex(e => e.num === elem[indictorGlobalSettings[nodeID].chart.mappedField]);
		if (i > -1) {
			elem['countryLabel'] = countries[i].name;
			elem['countryISO2'] = countries[i].iso2;
			return true;
		} else {
			//console.log("Country code: "+elem[indictorGlobalSettings[nodeID].chart.mappedField]+" is not in the ACP");
			return false; 
		}
	}
	if (chartType == "barLine"){
		if(indictorGlobalSettings[nodeID].chart.sort == "Ascending"){
			sortByKey(indicatorResults, indictorGlobalSettings[nodeID].chart.chartSeries[0].data, 'asc');
		} 
		if(indictorGlobalSettings[nodeID].chart.sort == "Descending"){
			sortByKey(indicatorResults, indictorGlobalSettings[nodeID].chart.chartSeries[0].data, 'desc');
		}
		//make a copy of the xaxis to modify
		var xAxis = jQuery.extend( true, {}, indictorGlobalSettings[nodeID].chart.Xaxis );
		console.log(xAxis);
		var xAxisLabel = xAxis.data.toLowerCase();

		//if it's a year field we sort it. This overrides what the user may have defined because it's important to standardise how the dates are displayed.
		if ((xAxis.data == "timePeriodStart") || (xAxisLabel == "year") || (xAxisLabel == "years")){
			sortByKey(indicatorResults, xAxis.data, 'asc');
		}
		//here we replace the x axis data placeholder with the array of values from the rest service. We only do this if they have assigned a value to that axis.
		if (typeof xAxis['data'] !== 'undefined'){
			var xAxisData = [], testDate, fullDate;
			if ((indictorGlobalSettings[nodeID].chart.mapLayerField  == 'un_m49') || (indictorGlobalSettings[nodeID].chart.mapLayerField  == 'iso3') || (indictorGlobalSettings[nodeID].chart.mapLayerField  == 'iso2')){
				//if the chart is linked to the map by a country field, then we replace the country code in the chart by the country name 
				indicatorResults.forEach(function(object){
					xAxisData.push(object['countryLabel']);
				});
			} else {
				indicatorResults.forEach(function(object){
					//get the date - it could be a simple year, e.g. 2012 or more complex, e.g. 2016/04/16 - cant get date types to work in echarts
					// testDate = object[xAxis['data']];
					// if (testDate.length === 4){ //assumption is that if the test date is 4 characters long then it is a year
					//   fullDate = new Date(testDate, 11, 31); //month index and date
					// }else{
					//   fullDate = new Date(testDate);
					// }
					// xAxisData.push(fullDate.toLocaleString());
					xAxisData.push(object[xAxis['data']]);
				});
			}
			xAxis['data'] = xAxisData;
		}
		var yAxis = jQuery.extend( true, {}, indictorGlobalSettings[nodeID].chart.Yaxis );
		//here we replace the y axis data placeholder with the array of values from the rest service. We only do this if they have assigned a vlaue to that axis.
		if (typeof yAxis['data'] !== 'undefined'){
			var yAxisData = [];
			indicatorResults.forEach(function(object){
				yAxisData.push(object[yAxis['data']]);
			});
			yAxis['data'] = yAxisData;
		}
		var mappedField = [];
		var countryLabels = [];
		var countryISO2codes = [];
		indicatorResults.forEach(function(object){
			if(indictorGlobalSettings[nodeID].chart.mapLayerField  == 'wdpaid'){
				mappedField.push(parseInt(object[indictorGlobalSettings[nodeID].chart.mappedField]));
			} else if (indictorGlobalSettings[nodeID].chart.mapLayerField  == 'Group'){
				switch(object[indictorGlobalSettings[nodeID].chart.mappedField]){
					case "western_africa":
						mappedField.push("Western Africa");
						break;
					case "southern_africa":
						mappedField.push("Southern Africa");
						break;
					case "eastern_africa":
						mappedField.push("Eastern Africa");
						break;
					case "central_africa":
						mappedField.push("Central Africa");
						break;
					default:
						mappedField.push(object[indictorGlobalSettings[nodeID].chart.mappedField]);
						break;
				}
			} else {
				mappedField.push(object[indictorGlobalSettings[nodeID].chart.mappedField]);
				countryLabels.push(object['countryLabel'])
				countryISO2codes.push(object['countryISO2'])
			}
		});
		//first we get the list of all data series defined in the indicator content type
		//we push these to the chartSeries array
		var chartSeries = [];
		
		//had a killer time trying to figure out why this damn thing wasn't cloning using other methods. Just leave it like this
		var data = JSON.parse(JSON.stringify(indictorGlobalSettings[nodeID].chart.chartSeries));
		for (var key2 in data) {
			// skip loop if the property is from prototype
			if (!data.hasOwnProperty(key2)) continue;
			var obj = data[key2];
			for (var prop in obj) {
				// skip loop if the property is from prototype
				if(!obj.hasOwnProperty(prop)) continue;
				if (prop=='data'){
					//replace the placeholder of the series map with the actual array from the rest service 
					//obj[prop] = indicatorResults[obj[prop]];
					chartSeries.push(obj[prop])
					//console.log(prop + " = " + obj[prop]);
				}
			}
		}
		var isChartSeriesNaN = [];
		//here we exchange the field labels in the array with arrays of the field values. 
		chartSeries.forEach(function(object, index){
			var chartData = [];
			//if it's the first in the series we Swap it out with NaN so it can remain compatible with the chart
			if (index == 0){
				indicatorResults.forEach(function(object2){
					var tempDataVal = object2[chartSeries[index]];
					tempDataVal = Number(tempDataVal);
					if (isNaN(tempDataVal)) {
						chartData.push("NaN");
						isChartSeriesNaN.push(1);
					} else {
						var dataVal = Math.round((tempDataVal + Number.EPSILON) * 100) / 100;
						chartData.push(dataVal);
						isChartSeriesNaN.push(0);
					}
				});
			} else {
				indicatorResults.forEach(function(object2){
					var tempDataVal = object2[chartSeries[index]];
					tempDataVal = Number(tempDataVal);
					var dataVal = Math.round((tempDataVal + Number.EPSILON) * 100) / 100;
					chartData.push(dataVal);
				});
			}
			chartSeries[index]=chartData;
		});
		//Now we got back into the original results and replace the series data with the arrays 
		//we push these to the chartSeries array
		for (var key2 in data) {
			// skip loop if the property is from prototype
			if (!data.hasOwnProperty(key2)) continue;
			var obj = data[key2];
			for (var prop in obj) {
				// skip loop if the property is from prototype
				if(!obj.hasOwnProperty(prop)) continue;
				if (prop=='data'){
					//replace the placeholder of the series map with the actual array from the rest service 
					obj[prop] = chartSeries[key2];
					//console.log(prop + " = " + obj[prop]);
				}
			}
		}
		
		data[0]['mapFeature'] = mappedField;
		data[0]['countryISO2'] = countryISO2codes;
		data[0]['countryLabels'] = countryLabels;
		data[0]['isNaN'] = isChartSeriesNaN;
		data[0]['emphasis'] = {itemStyle: {color: '#ff6600'}};		
		chartSettings[nodeID]['xAxis'] = xAxis;
		chartSettings[nodeID]['yAxis'] = yAxis;
		chartSettings[nodeID]['data'] = data;
	}
	if (chartType == "doughnut"){
		var allPieData = [];
		var tempPieValuesAggregated = 0;
		var pieSeries = JSON.parse(JSON.stringify(indictorGlobalSettings[nodeID].chart.chartSeries));
		
		switch(indictorGlobalSettings[nodeID].chart.pie.type){
			case 'Doughnut':
				chartSettings[nodeID]['roseType'] = false;
				chartSettings[nodeID]['radius'] = ['50%', '70%'];
				break;
			case 'RoseArea':
				chartSettings[nodeID]['roseType'] = 'area';
				chartSettings[nodeID]['radius'] = '50%';
				break;
			case 'RoseRadius':
				chartSettings[nodeID]['roseType'] = 'radius';
				chartSettings[nodeID]['radius'] = '50%';
				break;
			case 'Pie':
			default:
				chartSettings[nodeID]['roseType'] = false;
				chartSettings[nodeID]['radius'] = '50%';
				break;
		}

		var chartPieLimit = parseFloat(indictorGlobalSettings[nodeID].chart.pie.limit).toFixed(2);
		if (isNaN(chartPieLimit)) chartPieLimit = 0;
		
		var tempPieValuesAggregated = 0;
		
		var allPieData = [];

		//it's not great but for now we are checking for how DOPA does it and seperately for how ofac does it.
		pieSeries.slice(0, 10).forEach(function(pieSeriesObject, pieSeriesindex){
			if (Array.isArray(indicatorResults)) {
				indicatorResults.slice(0, 10).forEach(function(indicatorResultsObject, indicatorResultsIndex){
					var pieData = addPieChartData(indicatorResultsObject, pieSeriesObject, pieSeriesindex);
					allPieData.push(pieData);
				});
			} else {
				var pieData = addPieChartData(indicatorResults, pieSeriesObject);
				allPieData.push(pieData);
			}
		});
		function addPieChartData(chartData, pieSeries, dataIndex = 0){
			var pieData = {}//{value: indictorGlobalSettings[nodeID].chart.pie.fields, name: indictorGlobalSettings[nodeID].chart.pie.labels}
			pieData.value = chartData[pieSeries.data];
			pieData.name = pieSeries.name;
			tempPieValuesAggregated += +pieData.value
			var checkPieChartLabels = pieSeries.label;
			if (checkPieChartLabels.length >= 1) {
				if (pieSeries.label_from_rest == 1){
					pieData.label = {formatter: chartData[pieSeries.label]};
				} else {
					var cleanLabel = replaceLabelTokens(pieSeries.label);
					pieData.label = {formatter: cleanLabel};
				}	
			} else {
				pieData.labelLine = {show: false};
				pieData.emphasis = { labelLine: {show: false}};
			}
			return pieData;
		}
		function replaceLabelTokens(chartSeriesLabel){
			chartSeriesLabel = chartSeriesLabel.replace("[n]", "{b}");
			chartSeriesLabel = chartSeriesLabel.replace("[v]", "{c}");
			chartSeriesLabel = chartSeriesLabel.replace("[p]", "{d}");
			return chartSeriesLabel;
		}
		if (chartPieLimit > 0){
			var pieLimitCheck = chartPieLimit - tempPieValuesAggregated;
			var underLimitLabel = replaceLabelTokens(indictorGlobalSettings[nodeID].chart.pie.limitUnderLabel);
			var overLimitLabel = replaceLabelTokens(indictorGlobalSettings[nodeID].chart.pie.limitOverLabel);
			if (pieLimitCheck >= 0) {
				if (underLimitLabel.length >= 1) {
					allPieData.push({label: {formatter: underLimitLabel}, value: pieLimitCheck});
				} else {
					allPieData.push({value: pieLimitCheck,  labelLine: {show: false}, emphasis: { labelLine: {show: false}}});
				}
			} else {
				if (overLimitLabel.length >= 1) {
					allPieData.push({label: {formatter: overLimitLabel}, value: chartPieLimit});
				} else {
					allPieData.push({value: chartPieLimit,  labelLine: {show: false}, emphasis: { labelLine: {show: false}}});
				}
			}
		}
		chartSettings[nodeID]['data'] = allPieData;
		chartSettings[nodeID]['unit'] = indictorGlobalSettings[nodeID].chart.pie.unit ;
		
		
	}
	//jQuery().removeBiopamaLoader("#block-indicatorcards"); 
	return chartSettings[nodeID];
}
function checkForChartType(chartTab, nodeID){
	// jQuery().removeBiopamaLoader("indicator-card-tabs-"+nodeID); 

	// if (jQuery('.wrapper-ctt-data-card.show').length > 1) {
	// 	jQuery().removeBiopamaLoader(".wrapper-ctt-data-card.show"); 
	// }
	jQuery().removeBiopamaLoader("#view-wrapper-"+nodeID); 

	if ((jQuery( "div.field--name-field-indi-data-"+chartTab+" div.chart-bar-line" ).length) || (jQuery( "div.field--name-field-indi-get-data-from .field__item").text() == "BIOPAMA Geonode")){
		makeBarLineChart(nodeID);
		//jQuery().createXYAxisChart(cardTwigName , chartData, false, nodeID);  
	} 
	else if (jQuery( "div.field--name-field-indi-data-"+chartTab+" div.chart-2d-scatter" ).length){
		makeScatterChart(nodeID);
	} 
	else if (jQuery( "div.field--name-field-indi-data-"+chartTab+" div.chart-pie" ).length){
		makeDoughnutChart(nodeID);
	}
	else {
		//makeDoughnutChart();
		jQuery( ".rest-error" ).html("<div class='alert alert-info'>No Chart was Configured yet.</div>");
	}
}

function noChart(){
	//Any custom that we want to have at the country level can be done here.
	//we update the title
	jQuery("span.indicator-for").text("");
	jQuery("span.indicator-country").text("");
	jQuery( ".indicator-chart" ).hide();
}
function highlightMapFeature(nodeID) {
	if ((jQuery("[id*='indicator-card-tabs']").length)) {
		if (!indicatorCharts[nodeID]){
			return;
		} else {
			indicatorCharts[nodeID].dispatchAction({
				type: 'downplay',
				//It removes highlighting on upto 9 data series, it's unlikely there will be more...
				seriesIndex: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
			});
			var dataIndexKey = "";
			switch(indictorGlobalSettings[nodeID].chart.mapLayerField){
				case 'wdpaid':
					if (selSettings.WDPAID > 0){
						preformChartAction(selSettings.WDPAID);
					}
					if (pasCurrentlyHovered.length > 0){
						for (var key in pasCurrentlyHovered) {
							preformChartAction(pasCurrentlyHovered[key]);
						}
					}
					break;
				case 'iso2':
					if (selSettings.iso2 !== null){
						preformChartAction(selSettings.iso2);
					}
					if (countryCurrentlyHoverediso2 !== ''){
						preformChartAction(countryCurrentlyHoverediso2);
					}
					break;
				case 'iso3':
					if (selSettings.iso2 !== null){
						preformChartAction(selSettings.iso3);
					}
					if (countryCurrentlyHoverediso3 !== ''){
						preformChartAction(countryCurrentlyHoverediso3);
					}
					break;
				case 'un_m49':
					if (selSettings.iso2 !== null){
						preformChartAction(selSettings.NUM);
					}
					if (countryCurrentlyHoveredUn !== ''){
						preformChartAction(countryCurrentlyHoveredUn);
					}
					break;
				case 'Group':
					if (selSettings.regionID !== null){
						preformChartAction(selSettings.regionID);
					}
					if (regionCurrentlyHovered !== ''){
						preformChartAction(regionCurrentlyHovered);
					}
					break;
				default:
					//console.log("map feature not detected for highlight event");
					break;
			}
			if ((indictorGlobalSettings[nodeID].chart.mapLayerField == "wdpaid") && (selSettings.WDPAID > 0)){
				preformChartAction(selSettings.WDPAID);
			}
			if ((indictorGlobalSettings[nodeID].chart.mapLayerField == "wdpaid") && (pasCurrentlyHovered.length > 0)){
				for (var key in pasCurrentlyHovered) {
					preformChartAction(pasCurrentlyHovered[key]);
				}
			}
			function preformChartAction(indexKey){
				indicatorCharts[nodeID].dispatchAction({
					type: 'highlight',
					dataIndex: jQuery.inArray( indexKey, chartSettings[nodeID].data[0].mapFeature )
				});
				indicatorCharts[nodeID].dispatchAction({
					type: 'showTip',
					seriesIndex: 0,
					dataIndex: jQuery.inArray( indexKey, chartSettings[nodeID].data[0].mapFeature )
				});
			}
		}
	}
}

function getChartMappedFields(chart, dataSeries, nodeID){
	if (dataSeries == 1){
		var MappedSeriesArray = indictorGlobalSettings[nodeID].chartSeriesGlobal[0].data;
		//var MappedSeriesZoom = MappedSeriesArray.slice(chart.dataZoom[0].startValue, chart.dataZoom[0].endValue + 1);
		var MappedSeriesZoom = MappedSeriesArray.slice(chart.dataZoom[0].startValue, chart.dataZoom[0].endValue + 1);
		return MappedSeriesZoom;
	} else {
		//console.log("start: " + option.dataZoom[0].startValue + " End: " + option.dataZoom[0].endValue)
		var MappedFieldsArray = chart.series[0].mapFeature;
		var MappedFieldsZoom = MappedFieldsArray.slice(chart.dataZoom[0].startValue, chart.dataZoom[0].endValue + 1);
		return MappedFieldsZoom;
	}
}

function mapTheIndicator(mapData, nodeID){
	var chartOptions = indicatorCharts[nodeID].getOption();
	var MappedFieldsZoom = getChartMappedFields(chartOptions, 0, nodeID);
	var MappedSeriesZoom = getChartMappedFields(chartOptions, 1, nodeID);
	var mapFilter = buildFilter(MappedFieldsZoom, 'in', indictorGlobalSettings[nodeID].chart.mapLayerField);
	indictorGlobalSettings[nodeID].userPointToggle = 0;
	
	//if a card is open, and it has a toggle box, check it out.
	if (jQuery("#block-indicatorcard article:visible").length){
		if (jQuery('.indicator-make-points .form-control:visible').is(':checked')){
			indictorGlobalSettings[nodeID].userPointToggle = 1;
		} 
	}
	
	//we separate the types of map/chart connections into two types:
	//IF centroid is selected we will try to make a new layer of points...
	//ELSE we interact with the already present vecor layers
	if (indictorGlobalSettings[nodeID].userPointToggle == 1){
		mapPoints(mapFilter, mapData, nodeID)
	} else {
		/* if (chart3D == true){
			map3D(MappedFieldsZoom, MappedSeriesZoom)
		} else {
			
		} */
		map2D(MappedFieldsZoom, MappedSeriesZoom, nodeID)
	}
}

function map2D(MappedFieldsZoom, MappedSeriesZoom, nodeID = ""){
	removelayergroup(nodeID);
	var cardWrapper;
	if (nodeID != ""){
		cardWrapper = "#id-"+nodeID;
	}
	dataPoints = {0: [], 1: [], 2: [], 3: [], 4: [], 5: [], 6: [], 7: [], 8: []}
	
	if(!indictorGlobalSettings[nodeID].customNumClasses){
			indictorGlobalSettings[nodeID].customNumClasses = 3
	}
	if (indictorGlobalSettings[nodeID].mapCustomSwatchColors){
		//indictorGlobalSettings[nodeID].mapCustomSwatchColors
		var swatch = indictorGlobalSettings[nodeID].mapCustomSwatchColors;
		var tempCustomSwatch = swatch.split("-");
		if (indictorGlobalSettings[nodeID].customNumClasses == 2){
			indictorGlobalSettings[nodeID].mapCustomSwatchCat = 'diverging'
		} else {
			indictorGlobalSettings[nodeID].mapCustomSwatchCat = tempCustomSwatch[0]
		}
		var tempCustomSwatchNum = parseInt(tempCustomSwatch[1], 10)
		indictorGlobalSettings[nodeID].mapCustomSwatchNum = tempCustomSwatchNum;
	} else {
		indictorGlobalSettings[nodeID].mapCustomSwatchColors = "diverging-7"
	}
	
	if (indictorGlobalSettings[nodeID].mapCustomSwatchNum == 0){
		indictorGlobalSettings[nodeID].mapColors = colorbrewer.diverging[7][3];
	} else {
		indictorGlobalSettings[nodeID].mapColors = colorbrewer[indictorGlobalSettings[nodeID].mapCustomSwatchCat][indictorGlobalSettings[nodeID].mapCustomSwatchNum][indictorGlobalSettings[nodeID].customNumClasses];
	}
	var mapColorCopy = indictorGlobalSettings[nodeID].mapColors.slice();
	if (indictorGlobalSettings[nodeID].customColorInvert){
		if (indictorGlobalSettings[nodeID].customColorInvert){
			mapColorCopy.reverse(); 
		} 
	} 

	if (indictorGlobalSettings[nodeID].mapMarkers){
		resetMapPoints(nodeID);
	}
	//filter the map based on the user defined field in the indicator (filter)
	if (['un_m49', 'iso3', 'iso2', 'wdpaid', 'Group'].indexOf(indictorGlobalSettings[nodeID].chart.mapLayerField) >= 0) {
		var legendText = [];
		var tempLayer = biopamaGlobal.map.layers.country;
		var tempSource = 'BIOPAMA_Poly';
		if (indictorGlobalSettings[nodeID].chart.mapLayerField == "wdpaid") {
			tempLayer = biopamaGlobal.map.layers.WDPALatest;
			tempSource = "geospatial_jrc";
		}
		if (indictorGlobalSettings[nodeID].chart.mapLayerField == "Group") {
			tempLayer = biopamaGlobal.map.layers.subRegion;
			tempSource = 'BIOPAMA_Poly';
		}
		if (thisMap.getLayer("wdpaAcpFillHighlighted")) {
			thisMap.setPaintProperty("wdpaAcpFillHighlighted", "fill-opacity", 0);
		}
		//remove the layers that may exist that were previously added by this function.	
		
		//clean out the null/NaN values from the array and show them in the map as a second, non interactive layer.
		var nanCheck = 0;
		var nanCount = 0;
		var nanMapAreas = [];
		var MappedSeriesZoomWithoutNaN = [];
		var MappedFieldsZoomWithoutNaN = [];

		MappedSeriesZoom = MappedSeriesZoom.filter(function(elem, index, array) { 
			//console.log(MappedFieldsZoom[index] + array[index]);
			if (isNaN(elem)) {
				nanCheck = 1;
				nanCount++;
				nanMapAreas.push(MappedFieldsZoom[index]);
				return 'NaN';
			} else {
				MappedFieldsZoomWithoutNaN.push(MappedFieldsZoom[index]);
				MappedSeriesZoomWithoutNaN.push(elem);
				return elem;
			}
		});
		if (MappedSeriesZoomWithoutNaN.length <= 1){
			return
		}

		if (nanCheck == 1) {
			jQuery().addMapLayer(thisMap, "nan-layers", nodeID);
			if (tempLayer == biopamaGlobal.map.layers.country){
				thisMap.setFilter("country-nan-layer-"+nodeID, buildFilter(nanMapAreas, 'in', indictorGlobalSettings[nodeID].chart.mapLayerField));
				thisMap.setLayoutProperty("country-nan-layer-"+nodeID, 'visibility', 'visible');
				jQuery(cardWrapper + " #custom-map-legend-nan").show();
			} else if (tempLayer == biopamaGlobal.map.layers.WDPALatest){
				thisMap.setFilter("pa-nan-layer-"+nodeID, buildFilter(nanMapAreas, 'in', indictorGlobalSettings[nodeID].chart.mapLayerField));
				thisMap.setLayoutProperty("pa-nan-layer-"+nodeID, 'visibility', 'visible');
				jQuery(cardWrapper + " #custom-map-legend-nan").show();
			} else {
				removeCustomLayers("country-nan-layer-"+nodeID);
				removeCustomLayers("pa-nan-layer-"+nodeID);
			}
		} else {
			removeCustomLayers("country-nan-layer-"+nodeID);
			removeCustomLayers("pa-nan-layer-"+nodeID);
		}
		
		var stopPointMin = parseFloat(Math.min.apply(null, MappedSeriesZoomWithoutNaN), 10);
		var stopPointMax = parseFloat(Math.max.apply(null, MappedSeriesZoomWithoutNaN), 10);
		
		if (indictorGlobalSettings[nodeID].customStopPoints) {
			console.log("Custom Points "+ indictorGlobalSettings[nodeID].customStopPoints);
			indictorGlobalSettings[nodeID].selectedStopPoints = indictorGlobalSettings[nodeID].customStopPoints;
		} else {
			//console.log("Default Points");
			//We do 3 things. If nothing was defined: define it, if it was defined: use it, if it was over ridden: use that.
			if (!indictorGlobalSettings[nodeID].customClassMethod){
				indictorGlobalSettings[nodeID].customClassMethod = "Natural Breaks (Jenks)"; //if the card wasn't setup properly, use jenks as a default
			} 
			if (stopPointMin == stopPointMax){
				return;
			} else {
				indictorGlobalSettings[nodeID].selectedStopPoints = calcPointSatistics(MappedSeriesZoomWithoutNaN, indictorGlobalSettings[nodeID].customClassMethod, indictorGlobalSettings[nodeID].customNumClasses, stopPointMin, stopPointMax, nodeID);
			}
			
		}
		//MappedSeriesZoomWithoutNaN.sort(function(a, b){return a-b});
		var filteredLegendText = [];
		var filteredLegendColors = [];
		
		var stopPointsWithEnds = indictorGlobalSettings[nodeID].selectedStopPoints.slice();
		stopPointsWithEnds.unshift(stopPointMin)//adds min to beginning
		jQuery(stopPointsWithEnds).each(function(index) {
			if (stopPointsWithEnds[index+1] != null) {
				legendText.push("=> " + stopPointsWithEnds[index].toFixed(2) + " and < " + stopPointsWithEnds[index+1].toFixed(2));
			} else {
				legendText.push(" > " + stopPointsWithEnds[index].toFixed(2))
			}
		});	
		var uniqueMappedFieldsZoom = MappedFieldsZoomWithoutNaN.filter(function(elem, index, array) {
			if (array.indexOf(elem) == -1) MappedSeriesZoomWithoutNaN.splice(index, 1);
			return index === array.indexOf(elem);
		});
		var dataColorArray = [];
		jQuery(uniqueMappedFieldsZoom).each(function(i, data) {
			jQuery(stopPointsWithEnds).each(function(index) {
				if (stopPointsWithEnds[index+1] != null) {
					if ((MappedSeriesZoomWithoutNaN[i] >= stopPointsWithEnds[index]) && (MappedSeriesZoomWithoutNaN[i] < stopPointsWithEnds[index+1])){
						if(indictorGlobalSettings[nodeID].chart.mapLayerField == "wdpaid"){
							dataPoints[index].push(parseInt(data));
						} else{
							dataPoints[index].push(data);
						}
						dataColorArray.push(mapColorCopy[index]);
					}
				} else {
					if (MappedSeriesZoomWithoutNaN[i] >= stopPointsWithEnds[stopPointsWithEnds.length -1]){
						if(indictorGlobalSettings[nodeID].chart.mapLayerField == "wdpaid"){
							dataPoints[stopPointsWithEnds.length -1].push(parseInt(data));
						} else{
							dataPoints[stopPointsWithEnds.length -1].push(data);
						}
						dataColorArray.push(mapColorCopy[index]);
					}
				}
			});
		});
		//lert(indictorGlobalSettings[nodeID].chart.mapLayerField)
		indictorGlobalSettings[nodeID].paintProp = ["match", ["get", indictorGlobalSettings[nodeID].chart.mapLayerField]];
		for (var prop in dataPoints) {
			// skip loop if the property is from prototype
			if (!dataPoints.hasOwnProperty(prop)) continue;
			if (dataPoints[prop].length){ 
				indictorGlobalSettings[nodeID].paintProp.push(dataPoints[prop], mapColorCopy[prop]);
				filteredLegendText.push(legendText[prop]);
				filteredLegendColors.push(mapColorCopy[prop]);
			}
		}
		indictorGlobalSettings[nodeID].paintProp.push("rgba(0,0,0,0)");
		
		buildMapLegend(filteredLegendText, filteredLegendColors, nodeID);
		buildMapSlider(indictorGlobalSettings[nodeID].selectedStopPoints, mapColorCopy, stopPointMin, stopPointMax, nodeID);
		
		updateIndicatorChart(uniqueMappedFieldsZoom, dataColorArray, MappedSeriesZoomWithoutNaN, nanCount, nodeID);

		thisMap.addLayer({
			"id": "1nd1l4y3r"+nodeID,
			"type": "fill",
			"source": tempSource,
			"source-layer": tempLayer,
			"layout": {
				"visibility": "visible"
			},
			'paint': {
				'fill-outline-color': indictorGlobalSettings[nodeID].paintProp,
				'fill-color': indictorGlobalSettings[nodeID].paintProp,
				'fill-opacity': 1,
				
			}
		}, "countryBorders");
		
		thisMap.on('mousemove', "1nd1l4y3r"+nodeID, function (e) {
			if (e.features.length > 0) {
				if (jQuery(".indicator-chart:visible").length){
					highlightMapFeature(nodeID);
				}
			} 
		});
		thisMap.on("mouseleave", "1nd1l4y3r"+nodeID, function() {
			if (thisMap.getLayer("wdpaAcpHover")) {
				thisMap.setLayoutProperty("wdpaAcpHover", 'visibility', 'none');
			}
			if (jQuery(".indicator-chart:visible").length){
				highlightMapFeature(nodeID);
			}
		});
	} else if (['STATUS_YR'].indexOf(indictorGlobalSettings[nodeID].chart.mapLayerField) >= 0){
		thisMap.setFilter("wdpaAcpFillHighlighted", [ "all", buildFilter(MappedFieldsZoom, 'in', 'status_yr'), ['==', 'iso3', selSettings.iso3] ]);
	} else if (selSettings.WDPAID > 0){
		if (currentTab == "Local"){
			thisMap.setFilter("wdpaAcpFillHighlighted", [ "all", ['!=', 'wdpaid', selSettings.WDPAID], ['==', 'iso3', selSettings.iso3] ]);
			thisMap.setPaintProperty("wdpaAcpFillHighlighted", "fill-opacity", 0.5);
			//show layer again just incase it's been turned off by the point layer
			thisMap.setLayoutProperty("wdpaAcpFillHighlighted", 'visibility', 'visible');
		}	
	} else {
		thisMap.setPaintProperty("wdpaAcpFillHighlighted", "fill-opacity", 0.5);
		if (currentTab == "Local"){
			thisMap.setFilter("wdpaAcpFillHighlighted",['==', 'iso3', selSettings.iso3]);
			//show layer again just incase it's been turned off by the point layer
			thisMap.setLayoutProperty("wdpaAcpFillHighlighted", 'visibility', 'visible');	
		}
	}
}
function calcPointSatistics(MappedSeriesZoom, classMethod, numClasses, stopPointMin, stopPointMax, nodeID){

	indictorGlobalSettings[nodeID].selectedStopPoints = [];
	var stopGap = (1 / numClasses);
	var stopTotal = stopGap;

	//console.log(stopPointMin +" - "+stopPointMax)
	var loopNum = 1;
	switch(classMethod){
		case 'Equal Interval':
			var tempVal = (stopPointMax - stopPointMin) * stopGap;
			do {
				indictorGlobalSettings[nodeID].selectedStopPoints.push((equalDistribution(stopPointMin, tempVal, loopNum)));
				loopNum++;
			} while (loopNum < numClasses);
			break;
		case 'Standard Deviation':
			var avg = average(MappedSeriesZoom);
			var tempVal = (standardDeviation(avg, MappedSeriesZoom)/2);
			do {
				var checkLoop = isOdd(loopNum);
				if (checkLoop){
					if ((avg - (tempVal * loopNum)) < stopPointMin){
						indictorGlobalSettings[nodeID].selectedStopPoints.push(stopPointMin);
					} else{
						indictorGlobalSettings[nodeID].selectedStopPoints.push((avg - (tempVal * loopNum)));
					}
				} else {
					if ((avg - (tempVal * loopNum)) > stopPointMax){
						indictorGlobalSettings[nodeID].selectedStopPoints.push(stopPointMax);
					} else{
						indictorGlobalSettings[nodeID].selectedStopPoints.push((avg + (tempVal * loopNum)));
					}
				}
				loopNum++;
			} while (loopNum < numClasses);
			break;
		case 'Quantile':
			do {
				indictorGlobalSettings[nodeID].selectedStopPoints.push((Quantile(MappedSeriesZoom, stopTotal)));
				stopTotal = stopTotal + stopGap;
				if (stopTotal > 0.95) stopTotal =  1; //we round it off if it gets close to 1 to fix decimal errors....
			} while (stopTotal < 1);
			break;
		case 'Natural Breaks (Jenks)':
		default:
			indictorGlobalSettings[nodeID].selectedStopPoints = naturalBreaks(MappedSeriesZoom, parseInt(numClasses));
			indictorGlobalSettings[nodeID].selectedStopPoints = indictorGlobalSettings[nodeID].selectedStopPoints.slice(1,-1);	
	}
	return indictorGlobalSettings[nodeID].selectedStopPoints;
	
	function isOdd(num) { return num % 2;}
	function equalDistribution(min, q, loopNum) {
	  var base = min + (q * loopNum);
	  return base;
	}
	function Quantile(data, q) {
	  var pos = ((data.length) - 1) * q;
	  var base = Math.floor(pos);
	  var rest = pos - base;
	  if( (data[base+1]!==undefined) ) {
		return data[base] + rest * (data[base+1] - data[base]);
	  } else {
		return data[base];
	  }
	}
	function standardDeviation(avg, values){
	  var squareDiffs = values.map(function(value){
		var diff = value - avg;
		var sqrDiff = diff * diff;
		return sqrDiff;
	  });
	  var avgSquareDiff = average(squareDiffs);
	  var stdDev = Math.sqrt(avgSquareDiff);
	  return stdDev;
	}

	function average(data){
	  var sum = data.reduce(function(sum, value){
		return sum + value;
	  }, 0);
	  var avg = sum / data.length;
	  return avg;
	}
	function naturalBreaks(series, numClasses) {
		var mat1 = [];
		for ( var x = 0, xl = series.length + 1; x < xl; x++) {
			var temp = []
			for ( var j = 0, jl = numClasses + 1; j < jl; j++) {
				temp.push(0)
			}
			mat1.push(temp)
		}

		var mat2 = []
		for ( var i = 0, il = series.length + 1; i < il; i++) {
			var temp2 = []
			for ( var c = 0, cl = numClasses + 1; c < cl; c++) {
				temp2.push(0)
			}
			mat2.push(temp2)
		}

		for ( var y = 1, yl = numClasses + 1; y < yl; y++) {
			mat1[0][y] = 1
			mat2[0][y] = 0
			for ( var t = 1, tl = series.length + 1; t < tl; t++) {
				mat2[t][y] = Infinity
			}
			var v = 0.0
		}

		for ( var l = 2, ll = series.length + 1; l < ll; l++) {
			var s1 = 0.0
			var s2 = 0.0
			var w = 0.0
			for ( var m = 1, ml = l + 1; m < ml; m++) {
				var i3 = l - m + 1
				var val = parseFloat(series[i3 - 1])
				s2 += val * val
				s1 += val
				w += 1
				v = s2 - (s1 * s1) / w
				var i4 = i3 - 1
				if (i4 != 0) {
					for ( var p = 2, pl = numClasses + 1; p < pl; p++) {
						if (mat2[l][p] >= (v + mat2[i4][p - 1])) {
							mat1[l][p] = i3
							mat2[l][p] = v + mat2[i4][p - 1]
						}
					}
				}
			}
			mat1[l][1] = 1
			mat2[l][1] = v
		}

		var k = series.length
		var kclass = []

		for (i = 0, il = numClasses + 1; i < il; i++) {
			kclass.push(0)
		}

		kclass[numClasses] = parseFloat(series[series.length - 1])

		kclass[0] = parseFloat(series[0])
		var countNum = numClasses
		while (countNum >= 2) {
			var id = parseInt((mat1[k][countNum]) - 2)
			kclass[countNum - 1] = series[id]
			k = parseInt((mat1[k][countNum] - 1))

			countNum -= 1
		}

		if (kclass[0] == kclass[1]) {
			kclass[0] = 0
		}

		range = kclass;
		range.sort(function (a, b) { return a-b })

		return range; //array of breaks
	}
}
function updateIndicatorChart(seriesData, colors, MappedSeriesZoom, nanCount = 0, nodeID){

	var newchartSettings = jQuery.extend( true, {}, chartSettings[nodeID] );

	jQuery(newchartSettings.data[0].data).each(function(index, chartGlobalPlace) {
			newchartSettings.data[0].data[index] = {
				value: 0,
				itemStyle: {color: '#fff' },
			}
	});
	jQuery(seriesData).each(function(i, chartProccessedPlace) {
		jQuery(chartSettings[nodeID].data[0].mapFeature).each(function(index, chartGlobalPlace) {
			if (chartProccessedPlace == chartGlobalPlace){
				var tempVal = chartSettings[nodeID].data[0].data[index];
				newchartSettings.data[0].data[index] = {
					value: tempVal,
					itemStyle: {color: colors[i] },
				}
				return false;
			}
		});
	});

	var option = {
		series: newchartSettings.data
	};
	indicatorCharts[nodeID].setOption(option);
}
function removelayergroup(nodeID){
	var currentLayers = thisMap.style._layers;
	for (var key in currentLayers) { 
		if (currentLayers[key].id.indexOf('1nd1l4y3r'+nodeID) != -1) {
			thisMap.removeLayer(currentLayers[key].id);
		} 
	}
}

function togglelayergroup(visibility, nodeID){
	var currentLayers = thisMap.style._layers;
	for (var key in currentLayers) { 
		if (currentLayers[key].id.indexOf('1nd1l4y3r'+nodeID) != -1) {
			thisMap.setLayoutProperty(currentLayers[key].id, 'visibility', visibility);
		} 
	}
}

function buildMapLegend(legText, legColor, nodeID){
	var cardWrapper = "#indicator-card-tabs-" + nodeID;
	var s = document.getElementById('custom-map-legend-'+nodeID).style;
	s.display="flex";
	jQuery("#custom-map-legend-" + nodeID + " .legend-unit").remove();
	jQuery(legText).each(function(index) {
		var legendUnit = "<div class='legend-unit d-flex flex-column'>"+
			"<div class='legend-color' style='background-color: "+legColor[index]+"; height:20px;'></div>"+
			"<div class='legend-text'>&nbsp;"+legText[index]+"</div>"+
		"</div>";
		jQuery("#custom-map-legend-" + nodeID).append( legendUnit );
	});	
	if(jQuery("#map-legend-title-"+nodeID).length == 0){
		jQuery( "<div class='map-legend-title' id='map-legend-title-"+nodeID+"'>"+indictorGlobalSettings[nodeID].chart.chartSeries[0].name+"</div>" ).insertBefore("#custom-map-legend-" + nodeID);
	} else {
		s = document.getElementById("map-legend-title-"+nodeID).style;
		s.display="unset";
	}
}
function buildMapSlider(numClasses, colors, minRange, maxRange, nodeID){
	var cardWrapper = "#id-" + nodeID;
	//#indicator-card-tabs" + nodeID

	//attach a new one to the visible chart
	jQuery("#color-swatch-wrapper-"+nodeID+" .ramps-diverging, #color-swatch-wrapper-"+nodeID+" .ramps-sequential").empty();
	var selectedNumClasses = numClasses.length + 1;
	
	jQuery(cardWrapper + " .class-method").val(indictorGlobalSettings[nodeID].customClassMethod)

	if (indictorGlobalSettings[nodeID].customColorInvert){
		jQuery(cardWrapper + " .color-reverse").prop("checked",true);
	} 
	if (indictorGlobalSettings[nodeID].customClassMethod == "Standard Deviation"){
		jQuery(cardWrapper + " .number-of-classess").val(3).attr('disabled', 'disabled');
	} else {
		jQuery(cardWrapper + " .number-of-classes:disabled").prop("disabled", false);
	}
	jQuery(cardWrapper + " .number-of-classes").val(indictorGlobalSettings[nodeID].customNumClasses);
	var selectedNumClasses = indictorGlobalSettings[nodeID].customNumClasses;
	for ( var i in colorbrewer.diverging ){
		var ramp = jQuery("<div class='ramp colorbrewdiverging-"+ i +"'></div>"),
			svg = "<svg width='20' height='"+selectedNumClasses*20+"'>";
		for ( var n = 0; n < selectedNumClasses; n++ ){
			svg += "<rect fill="+colorbrewer.diverging[i][selectedNumClasses][n]+" width='20' height='20' y='"+n*20+"'/>";
		}
		svg += "</svg>";
		jQuery("#color-swatch-wrapper-"+nodeID+" .ramps-diverging").append(ramp.append(svg).click( function(){
			if ( jQuery(this).hasClass("selected") ) return;
			setScheme( jQuery(this).attr("class").substr(14), nodeID );
			jQuery("#color-swatch-wrapper-"+nodeID).removeClass("show");
		}));
	}
	for ( var i in colorbrewer.sequential ){
		var ramp = jQuery("<div class='ramp colorbrewsequential-"+ i +"'></div>"),
			svg = "<svg width='20' height='"+selectedNumClasses*20+"'>";
		for ( var n = 0; n < selectedNumClasses; n++ ){
			svg += "<rect fill="+colorbrewer.sequential[i][selectedNumClasses][n]+" width='20' height='20' y='"+n*20+"'/>";
		}
		svg += "</svg>";
		jQuery("#color-swatch-wrapper-"+nodeID+" .ramps-sequential").append(ramp.append(svg).click( function(){
			if ( jQuery(this).hasClass("selected") ) return;
			setScheme( jQuery(this).attr("class").substr(14), nodeID );
			jQuery("#color-swatch-wrapper-"+nodeID).removeClass("show");
		}));
	}
	if (indictorGlobalSettings[nodeID].mapCustomSwatchColors){
		setScheme( indictorGlobalSettings[nodeID].mapCustomSwatchColors, nodeID );
	}
	function setScheme(selectedScheme, nodeID){
		var cardWrapper = "#id-"+nodeID;
		jQuery("#color-swatch-wrapper-"+nodeID+" .ramp.selected").removeClass("selected");
		jQuery(cardWrapper + " .collapse.show .active-color-selected").empty();
		jQuery("#color-swatch-wrapper-"+nodeID+" .ramp.colorbrew"+selectedScheme).clone().appendTo( cardWrapper + " .collapse.show .active-color-selected" ).addClass("card-selected-color");
		jQuery(cardWrapper + " .ramp.colorbrew"+selectedScheme).addClass("selected");
		if (indictorGlobalSettings[nodeID].mapCustomSwatchColors !== selectedScheme){
			indictorGlobalSettings[nodeID].mapCustomSwatchColors = selectedScheme;
			mapTheIndicator(chartSettings[nodeID].data, nodeID);
		}
	}

	
	var slider = document.getElementById('slider-color-'+nodeID);
	
	var connects = [true];
	jQuery(numClasses).each(function(i, data) {
		connects.push(true);
	});
	if (jQuery("#slider-color-"+nodeID).hasClass("noUi-target")){
		slider.noUiSlider.destroy();
	}

	noUiSlider.create(slider, {
		orientation: "vertical",
		direction: 'rtl',
		//tooltips: true,
		start: numClasses,
		connect: connects,
		range: {
			'min': [minRange],
			'max': [maxRange]
		}
	});
	
	jQuery(cardWrapper + " .noUi-connect").each(function(i, data) {
		jQuery(this).css('background', colors[i]);
	});
	slider.noUiSlider.on('change', function (values) { 
		var result=values.map(Number);
		indictorGlobalSettings[nodeID].customStopPoints = result;
		mapTheIndicator(chartSettings[nodeID].data, nodeID);
	});
	jQuery(cardWrapper + " .number-of-classes").change(function() {
		indictorGlobalSettings[nodeID].customNumClasses = jQuery( this ).val();
		if (indictorGlobalSettings[nodeID].customNumClasses == 0){
			indictorGlobalSettings[nodeID].customNumClasses = 3;
		} 
		mapTheIndicator(chartSettings[nodeID].data, nodeID);
	});
	jQuery(cardWrapper + " .color-reverse").change(function() {
		if (jQuery(this).prop("checked")) {
			indictorGlobalSettings[nodeID].customColorInvert = true;
		} else{
			indictorGlobalSettings[nodeID].customColorInvert = false;
		}
		mapTheIndicator(chartSettings[nodeID].data, nodeID);
	});
	jQuery(cardWrapper + " .class-method").change(function() {
		indictorGlobalSettings[nodeID].customStopPoints = null;
		if (indictorGlobalSettings[nodeID].customClassMethod !== jQuery( this ).val()){
			indictorGlobalSettings[nodeID].customClassMethod = jQuery( this ).val();
			if (indictorGlobalSettings[nodeID].customClassMethod == "Standard Deviation"){
				indictorGlobalSettings[nodeID].customNumClasses = 3;
			}
			mapTheIndicator(chartSettings[nodeID].data, nodeID);
		}
	});
}

function removeCustomLayers(level = '-b10p4m4'){
	//other levels include -gl0b4l, -r3g10n, -c0untry, -l0c4l == these look funny because they HAVE to be super unique
	var currentLayers = thisMap.style._layers;
	for (var key in currentLayers) { 
		if (currentLayers[key].id.indexOf(level) != -1) {
			var thisLayerID = currentLayers[key].id;
			thisMap.removeLayer(thisLayerID);
			if (thisMap.getSource(thisLayerID)) {
				thisMap.removeSource(thisLayerID);
			}
		} 
	}
}
function map3D(MappedFieldsZoom, MappedSeriesZoom){
	//make the map
	//loop through all the records in the response and create a layer that shows the feature.
	jQuery(MappedFieldsZoom).each(function(i, data) {
		thisMap.addLayer({
			"id": "indilayer3D"+indictorGlobalSettings[nodeID].info.name+i,
			"type": "fill-extrusion",
			"source": 'composite',
			'filter': ["all",
			['==', 'iso3', selSettings.iso3],
			['==', 'wdpaid', data]],
			"source-layer": "ACP_Poly-ch9f72",
			"layout": {
				"visibility": "visible"
			},
			'paint': {
				'fill-extrusion-color':  '#aaa',
				'fill-extrusion-height': MappedSeriesZoom[i]*5000,
				'fill-extrusion-base': 0,
				'fill-extrusion-opacity': 0.5
			}
		}, 'countryBorders');
	});
}

function mapPoints(mapFilter, mapData, nodeID){
	removelayergroup(nodeID);
	if (indictorGlobalSettings[nodeID].mapMarkers.length > 0){
		resetMapPoints(nodeID);
	}
	var arrDataMin = Math.min.apply(null, mapData[0].data),
	arrDataMax = Math.max.apply(null, mapData[0].data);
	var normalisedArr = mapData[0].data.map(normalize(arrDataMin, arrDataMax))
	var features = thisMap.querySourceFeatures('BIOPAMA_Poly', { sourceLayer: biopamaGlobal.map.layers.pa, filter: [ "all", mapFilter, ['==', 'iso3', selSettings.iso3]] });
	features.forEach(function(marker) {
	  // create a HTML element for each feature
	  var el = document.createElement('div');
	  el.className = 'marker';
	  var dataPosition = jQuery.inArray( marker.properties.wdpaid , mapData[0].mapFeature );
	  el.style='width:'+ normalisedArr[dataPosition] +'px;height:'+ normalisedArr[dataPosition] +'px;background:rgba(255, 152, 0, 0.5);border:1px solid rgba(255, 152, 0, 1);';
	  el.classList.add("wdpa-id-" + mapData[0].mapFeature[dataPosition]);

	  // make a marker for each feature and add to the map
	  var marker = new mapboxgl.Marker(el)
	  .setLngLat({lng: marker.properties.CNTX, lat: marker.properties.CNTY});
	  
	  indictorGlobalSettings[nodeID].mapMarkers.push(marker);
	});
	
	indictorGlobalSettings[nodeID].mapMarkers.forEach(function(marker) {
		marker.addTo(thisMap);
	});
	thisMap.setLayoutProperty("wdpaAcpFillHighlighted", 'visibility', 'none');
}

function normalize(min, max) {
	var delta = max - min;
	return function (val) {
		return ((val - min) / delta) * 20 + 10;
	};
}

function resetMapPoints(nodeID){
	if (indictorGlobalSettings[nodeID] !== undefined){
		if (indictorGlobalSettings[nodeID].mapMarkers){
			indictorGlobalSettings[nodeID].mapMarkers.forEach(function(marker) {
				marker.remove();
			});
			indictorGlobalSettings[nodeID].mapMarkers = [];
		}
	}
}

function prepGlobalChart(nodeID){
	if (indictorGlobalSettings[nodeID].map.mapScope != null) {
		//we update the Country Name in the card
		jQuery("span.indicator-country").text(" "+biopamaGlobal.regions[0].name)
	} else {
		echarts.dispose(document.getElementById('indicator-chart-global-'+nodeID));
		jQuery("span.indicator-country").text(" Global")
		jQuery("#rest-error-country-"+nodeID).html("<div class='alert alert-info'>Theis is a global dataset, not restricted to the ACP.</div>");
	}
	checkForChartType("global", nodeID);
}

function prepRegionalChart(nodeID){
	//Any custom that we want to have at the country level can be done here.
	if (selSettings.regionName != null) {
		//we update the region name in the card
		jQuery("span.indicator-country").text(selSettings.regionName)
		checkForChartType("regional", nodeID);
	} else {
		//if there's a previously created region chart, kill it
		echarts.dispose(document.getElementById('indicator-chart-regional-'+nodeID));
		jQuery("#indicator-chart-regional-"+nodeID ).hide();
		jQuery("#rest-error-regional-"+nodeID ).html("<div class='alert alert-info'>There is no Region currently selected, Try selecting one on the map.</div>");
	}
}

function prepCountryChart(nodeID){
	//Any custom that we want to have at the country level can be done here.
	if (selSettings.iso2 != null) {
		//we update the Country Name in the card
		jQuery("span.indicator-country").text(selSettings.countryName)
		checkForChartType("national", nodeID);
	} else {
		//if there's a previously created country chart, kill it
		echarts.dispose(document.getElementById('indicator-chart-national-'+nodeID));
		jQuery( "#indicator-chart-national-"+nodeID ).hide();
		jQuery( "#rest-error-national-"+nodeID ).html("<div class='alert alert-info'>There is no Country currently selected, Try selecting one on the map.</div>");
	}
}

function prepPaChart(nodeID){
	if (selSettings.WDPAID > 0){
		jQuery("span.indicator-country").text(selSettings.paName)
		checkForChartType("local", nodeID);
	} else {
		//if there's a previously created pa chart, kill it
		echarts.dispose(document.getElementById('indicator-chart-local-'+nodeID));
		jQuery( "#indicator-chart-local-"+nodeID ).hide();
		jQuery( "#rest-error-local-"+nodeID ).html("<div class='alert alert-info'>There is no Protected Area currently selected, Try selecting one on the map.</div>");
	}
}