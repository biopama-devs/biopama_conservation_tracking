
//

function makeDoughnutChart(nodeID){
	var indicatorURL = indictorGlobalSettings[nodeID].chart.RESTurl;
	var chartColors = ['#5470c6', '#91cc75', '#fac858', '#ee6666', '#73c0de', '#3ba272', '#fc8452', '#9a60b4', '#ea7ccc'];
	if (indicatorURL.includes("observatoire-comifac.net")){
		chartColors = ['#8fc04f', '#ae0000'];
	}

	chartSettings[nodeID] = checkChartData("doughnut", nodeID);
	
	option = {
	  legend: {
		top: 'bottom'
	  },
	  color: chartColors,
	  toolbox: biopamaGlobal.chart.toolbox,
	  tooltip: {
		trigger: 'item',
		confine: true,
		extraCssText: 'width:auto; white-space:pre-wrap;',
		formatter: '{b}  {c}' + chartSettings[nodeID].unit
	  },
	  series: [
		{
		  type: 'pie',
		  radius: chartSettings[nodeID].radius,
		  center: ['50%', '50%'],
		  roseType: chartSettings[nodeID].roseType,
		  itemStyle: {
			borderRadius: 8
		  },
		  data: chartSettings[nodeID].data
		}
	  ]
	};
	indicatorCharts[nodeID].setOption(option); 
	return;
}