function makeBarLineChart(nodeID){
	chartSettings[nodeID] = checkChartData("barLine", nodeID);
	//the first time this runs it's 'clean' IE. I don't inject the color values. But then as it's getting updated after the first draw the color values are nested inside... 
	//so can only run it once (the first time)
	if (chartSettings[nodeID] === undefined) {
		return;
	}
	if (indictorGlobalSettings[nodeID].firstChartRun === undefined){
		indictorGlobalSettings[nodeID].firstChartRun = 1;
	}
	if (indictorGlobalSettings[nodeID].firstChartRun){
		indictorGlobalSettings[nodeID].firstChartRun = 0;
		indictorGlobalSettings[nodeID].chartSeriesGlobal = (chartSettings[nodeID].data).slice();
	}
	var option = {
		title: {
			text: indictorGlobalSettings[nodeID].info.name + " for \n" + selSettings.countryName,
			show: false,
			textStyle: {
				color: biopamaGlobal.chart.colors.text
			}
		}, 
		legend: biopamaGlobal.chart.legend,
		toolbox: biopamaGlobal.chart.toolbox,
		tooltip: {
			trigger: 'axis',
			axisPointer : {            
				type : 'shadow',        // 'line' | 'shadow'
			},
			confine: true,
			extraCssText: 'width:auto; white-space:pre-wrap;',
			enterable: true,
			formatter: function (params) {
				var tooltip = makeTTwMap(params, chartSettings[nodeID].data)
				return tooltip;
			}
		},
		xAxis: chartSettings[nodeID].xAxis,
		yAxis: chartSettings[nodeID].yAxis,
		// markLine: {
		// 	data: [ { name: 'xxxx', xAxis: 2000 } ]
		// },
		dataZoom: [
			{   // This dataZoom component controls x-axis by dafault
				type: 'slider', // this dataZoom component is dataZoom component of slider
				height: 20,
    			bottom: 10,		
			}
		],
		series: chartSettings[nodeID].data
	}; 
	option.xAxis['axisLine'] = { lineStyle: { color: biopamaGlobal.chart.colors.text}};
	option.yAxis['axisLine'] = { lineStyle: { color: biopamaGlobal.chart.colors.text}};
	
	indicatorCharts[nodeID].setOption(option); 

	highlightMapFeature(nodeID);

	var isMappable = chartSettings[nodeID].data[0].mapFeature[0];
	if (isMappable){
		if(thisMap){
			mapTheIndicator(chartSettings[nodeID].data, nodeID);

			indicatorCharts[nodeID].on('dataZoom', function (params) {
				mapTheIndicator(chartSettings[nodeID].data, nodeID);
			});
		}
	}

	function makeTTwMap(params, data){
		//console.log(params[0].axisValue)
		var toolTip = params[0].axisValue;
		params.forEach(function(element) {
			if (typeof element.data === 'object'){
				toolTip += "<br>" + element.seriesName + ": " + element.data.value;
			} else{
				toolTip += "<br>" + element.seriesName + ": " + element.data;
			}
			
		});
		if(thisMap){
			var mapFeature = data[0].mapFeature[params[0].dataIndex];
			if (mapFeature){
				switch(indictorGlobalSettings[nodeID].chart.mapLayerField){
					case 'wdpaid':
					case 'MARINE':
					case 'STATUS':
					case 'STATUS_YR':
					case 'DESIG_TYPE':
					case 'DESIG_ENG':
					case 'GOV_TYPE':
					case 'IUCN_CAT':
						thisMap.setFilter('wdpaAcpHover', ['all', ['==', (indictorGlobalSettings[nodeID].chart.mapLayerField).toLowerCase(), mapFeature]/* , ['==', 'iso3', selSettings.iso3] */]);
						thisMap.setLayoutProperty("wdpaAcpHover", 'visibility', 'visible');	
						break;
					case 'iso2':
					case 'iso3':
					case 'un_m49':
						thisMap.setFilter('countryHover', ['==', indictorGlobalSettings[nodeID].chart.mapLayerField, mapFeature]);
						thisMap.setLayoutProperty("countryHover", 'visibility', 'visible');	
						break;
					case 'Group':
						thisMap.setFilter("regionHover", ['==', indictorGlobalSettings[nodeID].chart.mapLayerField, mapFeature]);
						thisMap.setLayoutProperty("regionHover", 'visibility', 'visible');
						break;
					default:
						console.log("map feature not detected for chart hover event");
						break;
				}
				if (jQuery(".marker").length > 0 ){
					jQuery(".marker").css({ 'background': 'rgba(255, 152, 0, 0.5)', "border": "1px solid rgba(255, 152, 0, 1)" }); 
					jQuery(".wdpa-id-" + data[0].mapFeature[params[0].dataIndex]).css({ 'background': 'rgba(103, 155, 149, 0.6)', "border": "2px solid #679b95" });
				}	
			}
		}
		return toolTip;
	}

	indicatorCharts[nodeID].on('click', function (params) {
		if(thisMap){
			if (params.componentType === 'series'){
				var mapFeature = option.series[0].mapFeature[params.dataIndex];
				//TODO - instead if just highlighting on click, we should see if there's a tab we can navigate to based on the current selection.
				switch(indictorGlobalSettings[nodeID].chart.mapLayerField){
					case 'wdpaid':
						selSettings.WDPAID = mapFeature;
						thisMap.setFilter('wdpaAcpSelected', ['==', 'wdpaid', mapFeature]);
						thisMap.setLayoutProperty("wdpaAcpSelected", 'visibility', 'visible');
						paChanged = 1;
						if(currentPath.includes("/pa/")){
							updatePa()//this is the country page function to update the indicator
						} else {
							jQuery().mapZoomToPA(thisMap, mapFeature);
						}
						break;
					case 'iso2':
					case 'iso3':
					case 'un_m49':
						selSettings.iso2 = option.series[0].countryISO2[params.dataIndex];
						jQuery().updateSectedCountry();
						countryChanged = 1;
						var currentPath = window.location.pathname;
						updateCountry();
						break;
					case 'Group':
						selSettings.regionID = mapFeature;
						//regionChanged = 1;
						jQuery().mapZoomToRegion(thisMap, selSettings.regionID);
						break;
					default:
						console.log("map feature not detected for zoom event");
						break;
				}
				highlightMapFeature(nodeID);
			}
		}
	}); 
	
	jQuery(".indicator-content-wrapper").on('mouseout', function (params) {
		if(thisMap){
			if (thisMap.getLayer("wdpaAcpHover")) {
				thisMap.setLayoutProperty("wdpaAcpHover", 'visibility', 'none');
				if (jQuery(".marker").length > 0 ){
					jQuery(".marker").css({ 'background': 'rgba(255, 152, 0, 0.5)', "border": "1px solid rgba(255, 152, 0, 1)" }); 
				}	
			}
		}
		highlightMapFeature(nodeID);
	});
	return;
}

function makeScatterChart(nodeID){
	chartSettings[nodeID] = checkChartData("scatter", nodeID);
	var option = {
		legend: biopamaGlobal.chart.legend,
		toolbox: biopamaGlobal.chart.toolbox,
		tooltip: {
			trigger: 'axis',
			axisPointer : {            
				type : 'shadow',        // 'line' | 'shadow'
			},
			confine: false,
			enterable: true,
			formatter: function (params) {
				var tooltip = makeTTwMap(params, chartSettings[nodeID].data)
				return tooltip;
			}
		},
		xAxis: chartSettings[nodeID].xAxis,
		yAxis: chartSettings[nodeID].yAxis,
		dataZoom: [
			{   // This dataZoom component controls x-axis by dafault
				type: 'slider', // this dataZoom component is dataZoom component of slider
			}
		],
		series: chartSettings[nodeID].data
	}; 
	indicatorCharts[nodeID].setOption(option); 

	highlightMapFeature(nodeID);
	
	mapTheIndicator(chartSettings[nodeID].data, nodeID);

	indicatorCharts[nodeID].on('dataZoom', function (params) {
		mapTheIndicator(chartSettings[nodeID].data, nodeID);
	});

	function makeTTwMap(params, data){
		var toolTip = "";
		params.forEach(function(element) {
			toolTip += element.axisValueLabel + "<br>" + element.seriesName + ": " + element.data + "<br>"
		});
		var mapFeature = data[0].mapFeature[params[0].dataIndex];
		if (mapFeature){
			switch(indictorGlobalSettings[nodeID].chart.mapLayerField){
				case 'wdpaid':
					thisMap.setFilter('wdpaAcpHover', ['==', indictorGlobalSettings[nodeID].chart.mapLayerField, mapFeature]);
					thisMap.setLayoutProperty("wdpaAcpHover", 'visibility', 'visible');	
					break;
				case 'iso2':
				case 'iso3':
				case 'un_m49':
					thisMap.setFilter('countryHover', ['==', indictorGlobalSettings[nodeID].chart.mapLayerField, mapFeature]);
					thisMap.setLayoutProperty("countryHover", 'visibility', 'visible');	
					break;
				case 'Group':
					thisMap.setFilter("regionHover", ['==', indictorGlobalSettings[nodeID].chart.mapLayerField, mapFeature]);
					thisMap.setLayoutProperty("regionHover", 'visibility', 'visible');
					break;
				default:
					console.log("map feature not detected for chart hover event");
					break;
			}
			if (jQuery(".marker").length > 0 ){
				jQuery(".marker").css({ 'background': 'rgba(255, 152, 0, 0.5)', "border": "1px solid rgba(255, 152, 0, 1)" }); 
				jQuery(".wdpa-id-" + data[0].mapFeature[params[0].dataIndex]).css({ 'background': 'rgba(103, 155, 149, 0.6)', "border": "2px solid #679b95" });
			}	
		}
		return toolTip;
	}

	indicatorCharts[nodeID].on('click', function (params) {
		if (params.componentType === 'series'){
			var mapFeature = option.series[0].mapFeature[params.dataIndex];
			//TODO - instead if just highlighting on click, we should see if there's a tab we can navigate to based on the current selection.
			switch(indictorGlobalSettings[nodeID].chart.mapLayerField){
				case 'wdpaid':
					selSettings.WDPAID = mapFeature;
					thisMap.setFilter('wdpaAcpSelected', ['==', 'wdpaid', mapFeature]);
					thisMap.setLayoutProperty("wdpaAcpSelected", 'visibility', 'visible');
					paChanged = 1;
					jQuery().mapZoomToPA(thisMap, mapFeature);
					break;
				case 'iso2':
					selSettings.iso2 = mapFeature;
					countryChanged = 1;
					updateCountry('iso2');
					break;
				case 'iso3':
					selSettings.iso3 = mapFeature;
					countryChanged = 1;
					updateCountry('iso3');
					break;
				case 'un_m49':
					selSettings.num = mapFeature;
					countryChanged = 1;
					updateCountry('num');
					break;
				case 'Group':
					regionChanged = 1;
					$().mapZoomToRegion(thisMap, selSettings.regionID);
					break;
				default:
					console.log("map feature not detected for zoom event");
					break;
			}
			highlightMapFeature(nodeID);
		}
	}); 
	
	jQuery(".indicator-content-wrapper").on('mouseout', function (params) {
		thisMap.setLayoutProperty("wdpaAcpHover", 'visibility', 'none');
		if (jQuery(".marker").length > 0 ){
			jQuery(".marker").css({ 'background': 'rgba(255, 152, 0, 0.5)', "border": "1px solid rgba(255, 152, 0, 1)" }); 
		}	
		highlightMapFeature(nodeID);
	});
	return;
}