jQuery(document).ready(function($) {
	//this provides the ajax refresh of the views we have in the menu...
	//D9 patches have broken the feature formally using the Drupal API, now we delete and recreate the view manually.
	Drupal.behaviors.refreshMenuViews = {
		attach: function (context, settings) {
			$('#drupal-off-canvas').find('form.node-policy-form .alert-success, form.node-policy-edit-form .alert-success').once('updated-view').each( function() {
				console.log("policy saved");
				//$( ".view-id-menu_level_1_policies_:visible" ).trigger('RefreshView') 
				$( ".view-id-menu_level_1_policies_:visible" ).remove();
				var currentScope = $( ".active-scope" );
				if (currentScope.hasClass('global-trigger')){
					loadIndicators('global');
				} else if (currentScope.hasClass('regional-trigger')){
					loadIndicators('regional');
				} else if (currentScope.hasClass('national-trigger')){
					loadIndicators('national');
				} else if (currentScope.hasClass('local-trigger')){
					loadIndicators('local');
				}
				console.log("policy form closed");
				$("div.ui-dialog-titlebar button.ui-dialog-titlebar-close").delay( 800 ).trigger('click');
			});
			$('#drupal-off-canvas').find('form.node-goal-target-form .alert-success, form.node-goal-target-edit-form .alert-success').once('updated-view').each( function() {
				console.log("target saved");
				$( ".menu-goals:visible" ).trigger('RefreshView');
				$("div.ui-dialog-titlebar button.ui-dialog-titlebar-close").delay( 800 ).trigger('click');
			});
			$('#drupal-off-canvas').find('form.node-indicator-form .alert-success, form.node-indicator-edit-form .alert-success').once('updated-view').each( function() {
				$( ".menu-indicators:visible" ).trigger('RefreshView');
				if ($('#block-indicatorcard:visible').length){
					showIndicatorCard(currentIndicatorNodeURL);
				}
				$("div.ui-dialog-titlebar button.ui-dialog-titlebar-close").delay( 800 ).trigger('click');
			});	
			$('#drupal-off-canvas').find('form.node-indicator-data-glo-form .alert-success, form.node-indicator-data-regional-form .alert-success, form.node-indicator-data-country-form .alert-success, form.node-indicator-data-local-form .alert-success').once('updated-view').each( function() {
				if ($('#block-indicatorcard:visible').length){
					showIndicatorCard(currentIndicatorNodeURL);
				}
				$("div.ui-dialog-titlebar button.ui-dialog-titlebar-close").delay( 800 ).trigger('click');
			});	

		}
	};
});