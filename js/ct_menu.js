jQuery(document).ready(function($) {

    Drupal.behaviors.addCtMenuSearchEvents = {
		attach: function (context, settings) {
            $(context).find("#menu-indicator-search-cards-wrapper").once("add-search-behaviours").each(function () {
                addCTMenuSearchFunctions();
                var searchInput = $( "#ct-menu-search-filter .form-type-textfield input" );
                searchInput.focus(function() {
                    $( "div#menu-indicator-count" ).hide(200);
                    $( "div#ct-menu-search-count" ).show(200);
                    $( "div#menu-indicator-search-cards-wrapper" ).slideDown(200);
                });
                searchInput.focusout(delay(function (e) {
                    $( "div#menu-indicator-count" ).show(200);
                    $( "div#ct-menu-search-count" ).hide(200);
                    $( "div#menu-indicator-search-cards-wrapper" ).slideUp(200);
                }, 100));
                searchInput.focus();
                var tmpStr = searchInput.val();
                searchInput.val('');
                searchInput.val(tmpStr);
            });
		}
	};

    Drupal.behaviors.addCtMenuViewInteraction = {
		attach: function (context, settings) {
            $(context).find("#menu-indicator-cards-wrapper").once("add-menu-interaction").each(function () {
                addCTMenuInteraction();
            });
		}
	};

	//$(indicatorContainer).append("<div id='mini-loader-wrapper'><div id='mini-loader'></div></div>");
    if($("#menu-container").length){
        $( "#menu-container" ).ready(function() {
            addCTMenuInteraction();
            addCTMenuSearchFunctions();
        });
    }

    function addCTMenuSearchFunctions() {
        var searchTextField = $("#ct-menu-search-filter div.form-type-textfield input");
        var hiddenMenuSearchField = $( "#ct-menu-filters div.form-type-textfield input" );
        var hiddenSearchButton = $( "#ct-menu-search-filter .form-actions input.form-submit" );
        var menuSearchButton = $( "#ct-menu-filters .form-actions input.form-submit" );

        searchTextField.focus(function() {
            $("div#menu-indicator-search-cards-wrapper").slideDown(200);
        });
        searchTextField.keyup(delay(function (e) {
            hiddenSearchButton.trigger( "click" );
            hiddenMenuSearchField.val($(this).val().substring(0, 100));
            if (e.key === 'Enter' || e.keyCode === 13) {
                menuSearchButton.trigger( "click" );
            }
        }, 500));
        var tempVal= searchTextField.val();
        if (tempVal){
            tempVal = tempVal.substring(0, 100);
        }
        
        $('div#menu-indicator-search-cards .views-row .search-indicator-text').hover(
            function() {
                var indicatorSearchText = $(this).text().trim().substring(0, 100);
                searchTextField.val(indicatorSearchText);
                hiddenMenuSearchField.val(indicatorSearchText);
                return null;
            }, function () {
                searchTextField.val(tempVal);
                hiddenMenuSearchField.val(tempVal);
                return null;
            }
        );
        $('.search-policy-trigger').hover(
            function() {
                var indicatorSearchText = $(this).find(".search-policy-text").text().trim();
                searchTextField.val(indicatorSearchText);
                hiddenMenuSearchField.val(indicatorSearchText);
            }, function () {
                searchTextField.val(tempVal);
                hiddenMenuSearchField.val(tempVal);
            }
        );
        $('.search-target-trigger').hover(
            function() {
                var indicatorSearchText = $(this).find(".search-target-text").text().trim();
                searchTextField.val(indicatorSearchText);
                hiddenMenuSearchField.val(indicatorSearchText);
            }, function () {
                searchTextField.val(tempVal);
                hiddenMenuSearchField.val(tempVal);
            }
        );
        $('.indicator-search-card, .search-target-trigger').click( function() {
            $('div#menu-indicator-search-cards .views-row .search-indicator-text, .search-target-trigger, .search-policy-trigger').off(); //removes the events so the text doesn't change 
            $( "#ct-menu-search-filter .form-actions input.form-submit, #ct-menu-filters .form-actions input.form-submit" ).delay(200).trigger( "click" );
        });
    }

    function addCTMenuInteraction() {
        $('div.menu-indicator-card .menu-open-indicator').once("open-indicator").on( 'click', function() {
            var nodeID = $(this).closest("div.menu-indicator-card").find(".indicator-nid").text().trim();
            currentIndicatorNodeURL = '/indicator_card/'+nodeID;
            //console.log(currentIndicatorNodeURL)
            if($("#menuID-"+ nodeID).find("i.fa-plus").length){
                showIndicatorCard(currentIndicatorNodeURL);
            } else {
                closeIndicatorCard(nodeID);
            }
            
        });
    }

    function delay(callback, ms) {
        var timer = 0;
        return function() {
          var context = this, args = arguments;
          clearTimeout(timer);
          timer = setTimeout(function () {
            callback.apply(context, args);
          }, ms || 0);
        };
    }

    function showIndicatorCard(indicatorURL){
        var nid = indicatorURL.substring(indicatorURL.lastIndexOf('/') + 1);
        $().insertBiopamaLoader("#menuID-"+ nid); 
        //$( "#block-indicatorcards" ).empty();

        if ($("#id-"+ nid).length){
            closeIndicatorCard(nid);
        }
        $("#menuID-"+ nid).find("i.fa-plus").removeClass("fa-plus").addClass("fa-minus");
        $( '#block-indicatorcard' ).removeClass("d-none");
        
        var ajaxSettings = {
            url: indicatorURL,
            wrapper: 'block-indicatorcards',
            method: 'prepend',
          };
          var myAjaxObject = Drupal.ajax(ajaxSettings);
          myAjaxObject.execute();
    }
});
function closeIndicatorCards(){
    var $ = jQuery;
    $( '#block-indicatorcards .card.indicator-card' ).each(function( index ) {
        var tempID = this.id
        tempID = tempID.replace('id-', '');
        closeIndicatorCard(tempID);
    });
    $( '#block-indicatorcard' ).addClass("d-none");
}
function closeIndicatorCard(nodeID = 0){
    var $ = jQuery;

    var cardID = "#id-"+nodeID; //the open card in the map
    var menuID = "#menuID-"+nodeID; //the card in the menu

    $(".indicator-actions-icon").tooltip("hide");

    $(cardID).remove();
    if($( '#block-indicatorcards .card.indicator-card' ).length == 0) {
        $( '#block-indicatorcard' ).addClass("d-none");
    }
    
    if (nodeID !== 0){
        jQuery(menuID).find("i.fa-minus").removeClass("fa-minus").addClass("fa-plus");
    } else {
        jQuery("#menu-indicator-cards").find("i.fa-minus").removeClass("fa-minus").addClass("fa-plus");
    }

    if (thisMap){
        mapLayer = thisMap.getLayer("country-nan-layer-"+nodeID);
        if(typeof mapLayer !== 'undefined') {
            thisMap.setLayoutProperty("country-nan-layer-"+nodeID, "visibility", "none");
        }
        mapLayer = thisMap.getLayer("pa-nan-layer-"+nodeID);
        if(typeof mapLayer !== 'undefined') {
            thisMap.setLayoutProperty("pa-nan-layer-"+nodeID, "visibility", "none");
        }

        resetMapPoints(nodeID);
        removelayergroup(nodeID);
        
        //removes the card layers
        removeCustomLayers(nodeID);
        //removes whatever other layers from the tabs.
        removeCustomLayers('||a11');

        mapLayer = thisMap.getLayer('wdpaAcpFill');
        if(typeof mapLayer !== 'undefined') {
            thisMap.setLayoutProperty("wdpaAcpFill", 'visibility', 'visible');
            thisMap.setPaintProperty("wdpaAcpFillHighlighted", "fill-opacity", 0.6);
        }

        if (jQuery(".bread-trail-indicator:visible").length)jQuery(".bread-trail-indicator").one().toggle( "slide" );

        mapLayer = thisMap.getLayer('CountriesGreenMask');
        if(typeof mapLayer !== 'undefined') {
            thisMap.setLayoutProperty("CountriesGreenMask", 'visibility', 'none');
            thisMap.setLayoutProperty("CountriesRedMask", 'visibility', 'none');
        }
        //thisMap.setLayoutProperty("regionsFill", 'visibility', 'visible');
        mapLayer = thisMap.getLayer('wdpaAcpFillHighlighted');
        if(typeof mapLayer !== 'undefined') {
            if (selSettings.iso3 != null){
                thisMap.setFilter("wdpaAcpFillHighlighted", ['==', 'iso3', selSettings.iso3]);
                thisMap.setLayoutProperty("wdpaAcpFillHighlighted", 'visibility', 'visible');
            } else {
                thisMap.setLayoutProperty("wdpaAcpFillHighlighted", 'visibility', 'none');
            }
        }

        if (thisMap.getLayer("gbif-layer")) {
            thisMap.removeLayer("gbif-layer");
        }
        if (thisMap.getSource("gbif-source")) {
            thisMap.removeSource("gbif-source");
        }
    }

    
    
    customNumClasses = 0;
    if (indictorGlobalSettings[nodeID] !== undefined){
        indictorGlobalSettings[nodeID].firstChartRun = 1;
    }

    //thisMap.setLayoutProperty("eezACP", 'visibility', 'visible');
    //as there are no charts we turn off the 3D toggle option
    jQuery(".mapboxgl-ctrl-icon.mapboxgl-ctrl-chart-3D").addClass("disabled");

    jQuery('.wrapper-ctt-data-card > div.ctt-wrapper').empty();//acctually delete the indicator card as it will break the detection of the other CTT indicator variables if it stays.
    
}